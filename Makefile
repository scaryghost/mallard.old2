.PHONY: parser app engine

define build_sub_make_cmd
$(MAKE) -C $1
endef

PARSER_CMD = $(call build_sub_make_cmd, parser)
APP_CMD = $(call build_sub_make_cmd, app)
ENGINE_CMD = $(call build_sub_make_cmd, engine)

parser:
	$(PARSER_CMD)

app:
	$(APP_CMD)

engine:
	$(ENGINE_CMD)