#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>

#include "cxxopts.hpp"
#include "mallard/grammar/statement.hpp"
#include "mallard/reader/stream.hpp"

using namespace std;
using mallard::grammar::Statement;
using mallard::reader::StatementStream;

int main(int argc, char** argv) {
    cxxopts::Options options("mallard", "#1 programming langauge used by ducks");
    options.add_options()
        ("f,file", "Parse code from the filename", cxxopts::value<string>());
    auto result = options.parse(argc, argv);

    std::ifstream input(result["f"].as<string>());

    shared_ptr<Statement> statement;
    StatementStream stream(input);

    while(stream.has_next()) {
        stream >> statement;

        statement->flatten(cout);        
        cout << '\n';
    }
    
    cout << endl;
    return 0;
}