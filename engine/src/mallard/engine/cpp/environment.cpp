#include "mallard/engine/environment.hpp"
#include "mallard/engine/exception.hpp"
#include "mallard/engine/isa.hpp"

#include "mallard/engine/typing/function.hpp"
#include "mallard/engine/typing/integer.hpp"
#include "mallard/engine/typing/unit.hpp"

#include "mallard/grammar/assignment.hpp"
#include "mallard/grammar/function_application.hpp"
#include "mallard/grammar/operands.hpp"

#include <cstdint>
#include <sstream>
#include <stdexcept>
#include <string>
#include <stdexcept>
#include <utility>
#include <unordered_map>

using namespace std;

#define BUILD_BINARY_FN(type1, type2, op) op"_" + type1->name + "_" + type2->name
#define BUILD_BINARY_FN_ii(op) BUILD_BINARY_FN(typing::Integer::INSTANCE, typing::Integer::INSTANCE, op)

namespace mallard {
namespace engine {

Environment::Environment() : 
    binary_operations({
        {BUILD_BINARY_FN_ii("+"), &isa::add_ii},
        {BUILD_BINARY_FN_ii("-"), &isa::sub_ii},
        {BUILD_BINARY_FN_ii("*"), &isa::mul_ii},
        {BUILD_BINARY_FN_ii("/"), &isa::div_ii},
        {BUILD_BINARY_FN_ii(">"), &isa::gt_ii},
        {BUILD_BINARY_FN_ii(">="), &isa::gte_ii},
        {BUILD_BINARY_FN_ii("<"), &isa::lt_ii},
        {BUILD_BINARY_FN_ii("<="), &isa::lte_ii},
        {BUILD_BINARY_FN_ii("=="), &isa::eq_ii},
        {BUILD_BINARY_FN_ii("!="), &isa::neq_ii}
    })
{ }

Value Environment::eval(const shared_ptr<grammar::Statement>& statement) {    
    if (auto bin_expr = dynamic_pointer_cast<grammar::BinaryExpression>(statement)) {
        return eval_binary_expr(bin_expr);
    }

    if (auto int_literal = dynamic_pointer_cast<grammar::Integer>(statement)) {
        return eval_int_literal(int_literal);
    }

    if (auto identifier = dynamic_pointer_cast<grammar::Identifier>(statement)) {
        return eval_identifier(identifier);
    }

    if (auto assignment = dynamic_pointer_cast<grammar::Assignment>(statement)) {
        variables.insert({assignment->identifier, eval(assignment->expression)});

        return Value {
            shared_ptr<void>(nullptr),
            typing::Unit::INSTANCE
        };
    }

    if (auto fn = dynamic_pointer_cast<grammar::AnonymousFunction>(statement)) {
        return typing::Function::wrap(fn);
    }

    if (auto application = dynamic_pointer_cast<grammar::FunctionApplication>(statement)) {
        return eval_function_application(application);
    }

    stringstream msg;

    msg << "Statement type not supported: " << typeid(*statement).name();
    throw EngineException(msg.str());
}

Value Environment::eval_binary_expr(const std::shared_ptr<grammar::BinaryExpression>& binary_expr) {
    auto left = eval(binary_expr->left);
    auto right = eval(binary_expr->right);

    string fn_name = binary_expr->symbol + "_" + left.type->name + "_" + right.type->name;
    try {
        return binary_operations.at(fn_name)(left, right);
    } catch (const out_of_range&) {
        stringstream msg;
        msg << fn_name << " does not exist in the isa";

        throw_with_nested(EngineException(msg.str()));
    }
}

Value Environment::eval_int_literal(const std::shared_ptr<grammar::Integer>& int_literal) {
    return typing::Integer::INSTANCE->cast(int_literal);
}

Value Environment::eval_identifier(const std::shared_ptr<grammar::Identifier>& identifier) {
    try {
        return variables.at(identifier->name);
    } catch (const out_of_range&) {
        stringstream msg;
        msg << "Variable '" << identifier->name << "' not defined";

        throw_with_nested(EngineException(msg.str()));
    }
}

Value Environment::eval_function_application(const std::shared_ptr<grammar::FunctionApplication>& application) {
    Value fn;

    if (auto identifier = dynamic_pointer_cast<grammar::Identifier>(application->base_expression)) {
        fn = eval_identifier(identifier);
    } else if (auto function = dynamic_pointer_cast<grammar::AnonymousFunction>(application->base_expression)) {
        fn = typing::Function::wrap(function);
    } else if (auto base_application = dynamic_pointer_cast<grammar::FunctionApplication>(application->base_expression)) {
        fn = eval_function_application(base_application);
    } else {
        throw EngineException("Invalid expression type as function base");
    }

    for(auto& p: application->parameters) {
        fn = dynamic_pointer_cast<typing::Function>(fn.type)->beta_reduce(p);
    }

    auto anon_fn = fn.as<grammar::AnonymousFunction>();
    if (anon_fn->bound_variables.empty()) {
        Value result;

        for(auto &s: anon_fn->statements) {
            result = eval(s);
        }
        return result;
    }

    return fn;
}

}
}
