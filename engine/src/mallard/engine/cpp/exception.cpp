#include "mallard/engine/exception.hpp"

using std::string;
using mallard::exception::MallardException;

namespace mallard {
namespace engine {

EngineException::EngineException(const string& what_arg) : MallardException(what_arg) {

}

EngineException::EngineException(const char* what_arg) : MallardException(what_arg) {

}

EngineException::EngineException(const EngineException& other) noexcept : MallardException(other) {
    
}

}
}