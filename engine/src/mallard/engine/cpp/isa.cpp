#include "mallard/engine/isa.hpp"
#include "mallard/engine/stdlib.hpp"

#include "mallard/engine/typing/integer.hpp"

using namespace std;

namespace mallard {
namespace engine {
namespace isa {

Value add_ii(const Value& left, const Value& right) {
    return typing::Integer::INSTANCE->wrap(
        *static_pointer_cast<int32_t>(left.repr) + *static_pointer_cast<int32_t>(right.repr)
    );
}

Value sub_ii(const Value& left, const Value& right) {
    return typing::Integer::INSTANCE->wrap(
        *static_pointer_cast<int32_t>(left.repr) - *static_pointer_cast<int32_t>(right.repr)
    );
}

Value mul_ii(const Value& left, const Value& right) {
    return typing::Integer::INSTANCE->wrap(
        *static_pointer_cast<int32_t>(left.repr) * *static_pointer_cast<int32_t>(right.repr)
    );
}

Value div_ii(const Value& left, const Value& right) {
    return typing::Integer::INSTANCE->wrap(
        *static_pointer_cast<int32_t>(left.repr) / *static_pointer_cast<int32_t>(right.repr)
    );
}

Value eq_ii(const Value& left, const Value& right) {
    return stdlib::BOOLEAN[*static_pointer_cast<int32_t>(left.repr) == *static_pointer_cast<int32_t>(right.repr)];
}

Value neq_ii(const Value& left, const Value& right) {
    return stdlib::BOOLEAN[*static_pointer_cast<int32_t>(left.repr) != *static_pointer_cast<int32_t>(right.repr)];
}

Value lt_ii(const Value& left, const Value& right) {
    return stdlib::BOOLEAN[*static_pointer_cast<int32_t>(left.repr) < *static_pointer_cast<int32_t>(right.repr)];
}

Value lte_ii(const Value& left, const Value& right) {
    return stdlib::BOOLEAN[*static_pointer_cast<int32_t>(left.repr) <= *static_pointer_cast<int32_t>(right.repr)];
}

Value gt_ii(const Value& left, const Value& right) {
    return stdlib::BOOLEAN[*static_pointer_cast<int32_t>(left.repr) > *static_pointer_cast<int32_t>(right.repr)];
}

Value gte_ii(const Value& left, const Value& right) {
    return stdlib::BOOLEAN[*static_pointer_cast<int32_t>(left.repr) >= *static_pointer_cast<int32_t>(right.repr)];
}

}
}
}