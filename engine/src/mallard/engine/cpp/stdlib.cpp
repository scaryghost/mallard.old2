#include "mallard/engine/stdlib.hpp"
#include "mallard/engine/typing/function.hpp"

#include "mallard/grammar/anonymous_function.hpp"
#include "mallard/grammar/function_application.hpp"
#include "mallard/grammar/operands.hpp"

#include <initializer_list>
#include <memory>

using namespace std;
using namespace mallard::grammar;

namespace mallard {
namespace engine {
namespace stdlib {

const Value TRUE = typing::Function::wrap(
    make_shared<AnonymousFunction>(
        initializer_list<shared_ptr<Statement>>({ make_shared<Identifier>("x") }),
        initializer_list<shared_ptr<Identifier>>({ make_shared<Identifier>("x"), make_shared<Identifier>("y") })
    )
);

const Value FALSE = typing::Function::wrap(
    make_shared<AnonymousFunction>(
        initializer_list<shared_ptr<Statement>>({ make_shared<Identifier>("y") }),
        initializer_list<shared_ptr<Identifier>>({ make_shared<Identifier>("x"), make_shared<Identifier>("y") })
    )
);

const Value AND = typing::Function::wrap(
    make_shared<AnonymousFunction>(
        initializer_list<shared_ptr<Statement>>({ 
            make_shared<FunctionApplication>(
                make_shared<FunctionApplication>(
                    make_shared<Identifier>("p"),
                    initializer_list<shared_ptr<Expression>>({ make_shared<Identifier>("q") })
                ),
                initializer_list<shared_ptr<Expression>>({ make_shared<Identifier>("p") })
            )
        }),
        initializer_list<shared_ptr<Identifier>>({ make_shared<Identifier>("p"), make_shared<Identifier>("q") })
    )
);

const array<Value, 2> BOOLEAN = {FALSE, TRUE};

}
}
}
