#pragma once

#include "mallard/grammar/grammar.hpp"

#include "value.hpp"

#include <memory>
#include <string>
#include <unordered_map>

namespace mallard {
namespace engine {

struct Environment {
    Environment();

    Value eval(const std::shared_ptr<grammar::Statement>& statement);

private:
    Value eval_binary_expr(const std::shared_ptr<grammar::BinaryExpression>& binary_expr);
    Value eval_int_literal(const std::shared_ptr<grammar::Integer>& int_literal);
    Value eval_identifier(const std::shared_ptr<grammar::Identifier>& identifier);
    Value eval_function_application(const std::shared_ptr<grammar::FunctionApplication>& application);

    std::unordered_map<std::string, Value> variables;
    std::unordered_map<std::string, Value(*)(const Value&, const Value&)> binary_operations;
};

}
}