#pragma once

#include "mallard/exception/mallard_exception.hpp"

namespace mallard {
namespace engine {

class EngineException : public exception::MallardException {
public:
    EngineException(const std::string& what_arg);	
    EngineException(const char* what_arg);
    EngineException(const EngineException& other) noexcept;
};

}
}