#pragma once

#include "value.hpp"
#include "mallard/grammar/anonymous_function.hpp"

#include <memory>

namespace mallard {
namespace engine {
namespace isa {

Value add_ii(const Value& left, const Value& right);
Value sub_ii(const Value& left, const Value& right);
Value mul_ii(const Value& left, const Value& right);
Value div_ii(const Value& left, const Value& right);

Value eq_ii(const Value& left, const Value& right);
Value neq_ii(const Value& left, const Value& right);
Value lt_ii(const Value& left, const Value& right);
Value lte_ii(const Value& left, const Value& right);
Value gt_ii(const Value& left, const Value& right);
Value gte_ii(const Value& left, const Value& right);

}
}
}