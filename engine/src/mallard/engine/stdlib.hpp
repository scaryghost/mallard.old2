#pragma once

#include <array>

#include "mallard/engine/value.hpp"

namespace mallard {
namespace engine {
namespace stdlib {

extern const Value TRUE, FALSE, AND;
extern const std::array<Value, 2> BOOLEAN;

}
}
}