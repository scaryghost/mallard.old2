#include "mallard/engine/exception.hpp"
#include "mallard/engine/typing/function.hpp"

#include "mallard/grammar/binary_expression.hpp"
#include "mallard/grammar/function_application.hpp"
#include "mallard/grammar/operands.hpp"

#include <algorithm>
#include <cstdint>
#include <memory>
#include <sstream>
#include <string>
#include <utility>

#include <iostream>

using namespace std;

namespace mallard {
namespace engine {
namespace typing {

static shared_ptr<grammar::Statement> replace(const shared_ptr<grammar::Statement>& statement, const std::shared_ptr<grammar::Identifier>& variable, const shared_ptr<grammar::Expression> expr) {
    if (auto bin_expr = dynamic_pointer_cast<grammar::BinaryExpression>(statement)) {
        return make_shared<grammar::BinaryExpression>(
            dynamic_pointer_cast<grammar::Expression>(replace(bin_expr->left, variable, expr)),
            dynamic_pointer_cast<grammar::Expression>(replace(bin_expr->right, variable, expr)),
            bin_expr->symbol,
            bin_expr->priority
        );
    }

    if (auto identifier = dynamic_pointer_cast<grammar::Identifier>(statement)) {
        if (identifier->name == variable->name) {
            return expr;
        }
        return identifier;
    }

    if (auto int_literal = dynamic_pointer_cast<grammar::Integer>(statement)) {
        return statement;
    }
    
    if (auto application = dynamic_pointer_cast<grammar::FunctionApplication>(statement)) {
        vector<shared_ptr<grammar::Expression>> replaced_parameters;

        transform(
            application->parameters.begin(), application->parameters.end(), back_inserter(replaced_parameters), 
            [&variable, &expr](const shared_ptr<grammar::Expression>& e) -> shared_ptr<grammar::Expression> {
                return dynamic_pointer_cast<grammar::Expression>(replace(e, variable, expr));
            }
        );

        return make_shared<grammar::FunctionApplication>(
            dynamic_pointer_cast<grammar::Expression>(replace(application->base_expression, variable, expr)),
            replaced_parameters
        );
    }

    if (auto fn = dynamic_pointer_cast<grammar::AnonymousFunction>(statement)) {
        return statement;
    }

    stringstream msg;

    msg << "Statement type not supported in beta reduction: "  << typeid(*statement).name();
    throw EngineException(msg.str());
}

static string build_type_name(const shared_ptr<grammar::AnonymousFunction>& value) {
    stringstream msg;

    msg << "Function" << value->bound_variables.size();
    return msg.str();
}

Function::Function(const shared_ptr<grammar::AnonymousFunction>& value) : 
    Type(build_type_name(value)),
    value(value)
{

}

Function::~Function() {

}

Value Function::beta_reduce(const shared_ptr<grammar::Expression> expr) const {
    if (value->bound_variables.empty()) {
        throw EngineException("There are no bound variables in this function");
    }

    auto variable = value->bound_variables.front();
    vector<shared_ptr<grammar::Statement>> replaced_statements;
    vector<shared_ptr<grammar::Identifier>> remaining_vars;

    transform(
        value->statements.begin(), value->statements.end(), back_inserter(replaced_statements), 
        [&variable, &expr](const shared_ptr<grammar::Statement>& s) -> shared_ptr<grammar::Statement> {
            return replace(s, variable, expr);
        }
    );
    copy(next(value->bound_variables.begin()), value->bound_variables.end(), back_inserter(remaining_vars));

    return wrap(make_shared<grammar::AnonymousFunction>(replaced_statements, remaining_vars));
}

Value Function::wrap(const shared_ptr<grammar::AnonymousFunction>& value) {
    auto repr = shared_ptr<void>(nullptr, [](auto p) {
        delete (grammar::AnonymousFunction*) p;
    });
    repr = static_pointer_cast<void>(value);

    return Value {
        repr,
        make_shared<Function>(value)
    };
}

}
}
}