#include "mallard/engine/typing/integer.hpp"

#include <cstdint>
#include <string>

using namespace std;

namespace mallard {
namespace engine {
namespace typing {

const shared_ptr<Integer> Integer::INSTANCE = make_shared<Integer>();

Integer::Integer() : Type("int") {

}

Integer::~Integer() {

}

Value Integer::cast(const shared_ptr<grammar::Integer>& integer) const {
    return wrap(stoi(integer->value));
}

Value Integer::wrap(int32_t value) const {
    return Value {
        shared_ptr<void>(new int32_t(value), [](auto p) {
            delete (int32_t*) p;
        }),
        INSTANCE
    };
}

}
}
}