#include "mallard/engine/typing/unit.hpp"

using namespace std;

namespace mallard {
namespace engine {
namespace typing {

const shared_ptr<Unit> Unit::INSTANCE = make_shared<Unit>();

Unit::Unit() : Type("unit") {

}

Unit::~Unit() {

}

}
}
}