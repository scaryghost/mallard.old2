#pragma once

#include "type.hpp"

#include "mallard/engine/value.hpp"
#include "mallard/grammar/anonymous_function.hpp"
#include "mallard/grammar/expression.hpp"

#include <memory>

namespace mallard {
namespace engine {
namespace typing {

struct Function : public Type {
    static Value wrap(const std::shared_ptr<grammar::AnonymousFunction>& value);

    Function(const std::shared_ptr<grammar::AnonymousFunction>& value);
    virtual ~Function();

    Value beta_reduce(const std::shared_ptr<grammar::Expression> expr) const;

    const std::shared_ptr<grammar::AnonymousFunction> value;
};

}
}
}