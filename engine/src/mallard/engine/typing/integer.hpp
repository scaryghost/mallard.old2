#pragma once

#include "type.hpp"
#include "mallard/engine/value.hpp"
#include "mallard/grammar/operands.hpp"

#include <memory>

namespace mallard {
namespace engine {
namespace typing {

struct Integer : public Type {
    static const std::shared_ptr<Integer> INSTANCE;

    Integer();
    virtual ~Integer();

    Value cast(const std::shared_ptr<grammar::Integer>& integer) const;
    Value wrap(int32_t value) const;
};

}
}
}