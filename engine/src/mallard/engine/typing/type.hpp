#pragma once

#include <string>

namespace mallard {
namespace engine {
namespace typing {

struct Type {
    Type(const std::string& name);
    virtual ~Type();

    const std::string name;
};

}
}
}