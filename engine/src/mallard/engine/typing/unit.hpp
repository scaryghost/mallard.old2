#pragma once

#include "type.hpp"
#include "mallard/engine/value.hpp"
#include "mallard/grammar/operands.hpp"

#include <memory>

namespace mallard {
namespace engine {
namespace typing {

struct Unit : public Type {
    static const std::shared_ptr<Unit> INSTANCE;

    Unit();
    virtual ~Unit();
};

}
}
}