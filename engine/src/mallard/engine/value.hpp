#pragma once

#include "mallard/engine/typing/type.hpp"

#include <memory>

namespace mallard {
namespace engine {

struct Value {
    std::shared_ptr<void> repr;
    std::shared_ptr<typing::Type> type;

    template <class T>
    std::shared_ptr<T> as() const;
};

template <class T>
std::shared_ptr<T> Value::as() const {
    return std::static_pointer_cast<T>(repr);
}

}
}
