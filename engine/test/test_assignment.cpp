#include "catch2/catch.hpp"

#include "mallard/engine/environment.hpp"
#include "mallard/engine/typing/integer.hpp"

#include "mallard/grammar/assignment.hpp"
#include "mallard/grammar/binary_expression.hpp"
#include "mallard/grammar/operands.hpp"

#include <cstdint>
#include <string>
#include <tuple>

using namespace std;
using namespace mallard::engine;
using namespace mallard::grammar;

TEST_CASE("assign_int", "[engine_assignment]") {
    auto assignment = make_shared<Assignment>(
        "x",
        make_shared<Integer>("3")
    );

    auto env = Environment();
    env.eval(assignment);

    auto result = env.eval(make_shared<Identifier>("x"));

    CHECK(dynamic_pointer_cast<typing::Integer>(result.type));
    CHECK(*static_pointer_cast<int32_t>(result.repr) == 3);
}