#include "catch2/catch.hpp"

#include "mallard/engine/environment.hpp"
#include "mallard/engine/stdlib.hpp"
#include "mallard/engine/typing/function.hpp"
#include "mallard/engine/typing/integer.hpp"

#include "mallard/grammar/anonymous_function.hpp"
#include "mallard/grammar/function_application.hpp"
#include "mallard/grammar/operands.hpp"

#include <cstdint>
#include <initializer_list>
#include <string>
#include <tuple>

using namespace std;
using namespace mallard::engine;
using namespace mallard::grammar;

TEST_CASE("partial_application", "[engine_function_application]") {
    auto env = Environment();
    auto result = env.eval(make_shared<FunctionApplication>(
        stdlib::TRUE.as<AnonymousFunction>(),
        initializer_list<shared_ptr<Expression>>({ make_shared<Integer>("13") })
    ));
    auto fn = static_pointer_cast<AnonymousFunction>(result.repr);

    REQUIRE(dynamic_pointer_cast<typing::Function>(result.type));
    CHECK(fn->bound_variables.size() == 1);
    CHECK(fn->bound_variables[0]->name == "y");

    result = env.eval(make_shared<FunctionApplication>(
        fn,
        initializer_list<shared_ptr<Expression>>({ make_shared<Integer>("15") })
    ));

    REQUIRE(dynamic_pointer_cast<typing::Integer>(result.type));
    CHECK(*static_pointer_cast<int32_t>(result.repr) == 13);
}

TEST_CASE("logical_and", "[engine_function_application]") {
    auto data = GENERATE(values<tuple<Value, Value, Value>>({
        { stdlib::TRUE, stdlib::TRUE, stdlib::TRUE },
        { stdlib::TRUE, stdlib::FALSE, stdlib::FALSE },
        { stdlib::FALSE, stdlib::FALSE, stdlib::FALSE },
        { stdlib::FALSE, stdlib::TRUE, stdlib::FALSE },
    }));

    auto env = Environment();
    auto result = env.eval(make_shared<FunctionApplication>(
        stdlib::AND.as<AnonymousFunction>(),
        initializer_list<shared_ptr<Expression>>({ get<0>(data).as<AnonymousFunction>(), get<1>(data).as<AnonymousFunction>() })
    ));
    auto fn = result.as<AnonymousFunction>();
    
    REQUIRE(dynamic_pointer_cast<typing::Function>(result.type));
    CHECK(fn == get<2>(data).as<AnonymousFunction>());
}