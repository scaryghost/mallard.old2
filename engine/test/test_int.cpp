#include "catch2/catch.hpp"

#include "mallard/engine/environment.hpp"
#include "mallard/engine/stdlib.hpp"
#include "mallard/engine/typing/function.hpp"
#include "mallard/engine/typing/integer.hpp"

#include "mallard/grammar/anonymous_function.hpp"
#include "mallard/grammar/binary_expression.hpp"
#include "mallard/grammar/operands.hpp"

#include <cstdint>
#include <string>
#include <tuple>

using namespace std;
using namespace mallard::engine;
using namespace mallard::grammar;

TEST_CASE("ints", "[engine_int]") {
    auto data = GENERATE(values<tuple<const char*, const char*, const char*, int32_t>>({
        { "3", "5", "+", 8 },
        { "2", "6", "-", -4 },
        { "-1", "5", "*", -5 },
        { "10", "3", "/", 3 },
    }));

    auto left = make_shared<Integer>(get<0>(data));
    auto right = make_shared<Integer>(get<1>(data));
    auto expr = make_shared<BinaryExpression>(left, right, get<2>(data), 1);
    auto env = Environment();

    auto result = env.eval(expr);

    REQUIRE(dynamic_pointer_cast<typing::Integer>(result.type));
    CHECK(*static_pointer_cast<int32_t>(result.repr) == get<3>(data));
}

TEST_CASE("int_compare", "[engine_int]") {
    auto data = GENERATE(values<tuple<const char*, const char*, const char*, Value>>({
        { "-5", "5", "<", stdlib::TRUE },
        { "7", "0", "<=", stdlib::FALSE },
        { "3", "5", ">", stdlib::FALSE },
        { "13", "6", ">=", stdlib::TRUE },
        { "-1", "5", "==", stdlib::FALSE },
        { "10", "3", "!=", stdlib::TRUE },
    }));

    auto left = make_shared<Integer>(get<0>(data));
    auto right = make_shared<Integer>(get<1>(data));
    auto expr = make_shared<BinaryExpression>(left, right, get<2>(data), 1);
    auto env = Environment();

    auto result = env.eval(expr);

    REQUIRE(dynamic_pointer_cast<typing::Function>(result.type));
    CHECK(result.repr == get<3>(data).repr);
}