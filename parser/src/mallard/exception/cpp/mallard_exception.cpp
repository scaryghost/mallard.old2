#include "mallard/exception/mallard_exception.hpp"

using namespace std;

namespace mallard {
namespace exception {

MallardException::MallardException(const std::string& what_arg) : runtime_error(what_arg) {

}

MallardException::MallardException(const char* what_arg) : runtime_error(what_arg) {

}

MallardException::MallardException(const MallardException& other) noexcept : runtime_error(other) {
    
}

}
}