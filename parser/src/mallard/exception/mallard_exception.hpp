#pragma once

#include <stdexcept>
#include <string>

namespace mallard {
namespace exception {

class MallardException : public std::runtime_error {
public:
    MallardException(const std::string& what_arg);	
    MallardException(const char* what_arg);
    MallardException(const MallardException& other) noexcept;
};

}
}