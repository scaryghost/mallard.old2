#pragma once

#include "expression.hpp"
#include "operands.hpp"

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

namespace mallard {
namespace grammar {

struct AnonymousFunction : public Expression {
    AnonymousFunction(
        const std::vector<std::shared_ptr<Statement>>& statements, 
        const std::vector<std::shared_ptr<Identifier>>& bound_variables
    );
    virtual ~AnonymousFunction();

    virtual void flatten(std::ostream& os) const;
    virtual std::shared_ptr<Expression> clone() const;

    std::vector<std::shared_ptr<Statement>> statements;
    std::vector<std::shared_ptr<Identifier>> bound_variables;
};

}
}