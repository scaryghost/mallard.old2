#pragma once

#include "expression.hpp"
#include "statement.hpp"

#include <cstdint>
#include <memory>
#include <string>

namespace mallard {
namespace grammar {

struct Assignment : public Statement {
    Assignment(const std::string& identifier, const std::shared_ptr<Expression>& expression);
    virtual ~Assignment();

    virtual void flatten(std::ostream& os) const;

    std::shared_ptr<Expression> expression;
    std::string identifier;
};

}
}