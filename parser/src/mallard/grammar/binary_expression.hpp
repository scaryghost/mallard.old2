#pragma once

#include "expression.hpp"

#include <cstdint>
#include <memory>
#include <string>

namespace mallard {
namespace grammar {

struct BinaryExpression : public Expression {
    BinaryExpression(const std::shared_ptr<Expression>& left, const std::shared_ptr<Expression>& right, const std::string symbol, std::uint8_t priority);
    virtual ~BinaryExpression();

    virtual void flatten(std::ostream& os) const;
    virtual std::shared_ptr<Expression> clone() const;

    std::shared_ptr<BinaryExpression> current;
    std::shared_ptr<Expression>  left, right;
    std::string symbol;
    std::uint8_t priority;

    static std::shared_ptr<BinaryExpression> start_left(const std::shared_ptr<Expression> left, const std::string symbol, std::uint8_t priority);
};

}
}