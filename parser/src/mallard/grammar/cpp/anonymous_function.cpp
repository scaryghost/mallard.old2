#include "mallard/grammar/anonymous_function.hpp"

#include <algorithm>
#include <iterator>

using namespace std;

namespace mallard {
namespace grammar {

AnonymousFunction::AnonymousFunction(
    const vector<shared_ptr<Statement>>& statements, 
    const vector<shared_ptr<Identifier>>& bound_variables
) :
    statements(statements),
    bound_variables(bound_variables)
{

}

AnonymousFunction::~AnonymousFunction() {
}

void AnonymousFunction::flatten(ostream& os) const {
    if (!bound_variables.empty()) {
        os << "\\";
        bound_variables[0]->flatten(os);

        for_each(next(bound_variables.cbegin()), bound_variables.cend(), [&os](const shared_ptr<Identifier>& it) {
            os << " \\";
            it->flatten(os);
        });
    } else {
        os << "()";
    }

    os << " -> ";

    switch(statements.size()) {
    case 0:
        os << "{}";
        break;
    case 1:
        statements[0]->flatten(os);
        break;
    default:
        os << "{";

        for_each(statements.cbegin(), statements.cend(), [&os](const shared_ptr<Statement>& it) {
            os << "\n    ";
            it->flatten(os);
        });

        os << "\n}";
    }
}

shared_ptr<Expression> AnonymousFunction::clone() const {
    return make_shared<AnonymousFunction>(statements, bound_variables);
}

}
}