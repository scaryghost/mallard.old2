#include "mallard/grammar/assignment.hpp"

using std::make_shared;
using std::shared_ptr;
using std::string;

namespace mallard {
namespace grammar {

Assignment::Assignment(const string& identifier, const shared_ptr<Expression>& expression) :
    identifier(identifier),
    expression(expression)
{

}

Assignment::~Assignment() {
}

void Assignment::flatten(std::ostream& os) const {
    os << "val " << identifier << " = ";
    expression->flatten(os);
}

}
}