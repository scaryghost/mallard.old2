#include "mallard/grammar/binary_expression.hpp"

using std::make_shared;
using std::shared_ptr;
using std::string;

namespace mallard {
namespace grammar {

BinaryExpression::BinaryExpression(const std::shared_ptr<Expression>& left, const std::shared_ptr<Expression>& right, const std::string symbol, std::uint8_t priority) :
    left(left),
    right(right),
    symbol(symbol),
    priority(priority)
{

}

BinaryExpression::~BinaryExpression() {
}

shared_ptr<BinaryExpression> BinaryExpression::start_left(const shared_ptr<Expression> left, const string symbol, uint8_t priority) {
    return make_shared<BinaryExpression>(left, shared_ptr<Expression>(), symbol, priority);
}

void BinaryExpression::flatten(std::ostream& os) const {
    os << "(";
    left->flatten(os);
    os << " " << symbol << " ";
    right->flatten(os);
    os << ")";
}

shared_ptr<Expression> BinaryExpression::clone() const {
    return make_shared<BinaryExpression>(left, right, symbol, priority);
}

}
}