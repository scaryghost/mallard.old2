#include "mallard/grammar/function_application.hpp"

using std::shared_ptr;
using std::vector;

namespace mallard {
namespace grammar {

FunctionApplication::FunctionApplication(
    const shared_ptr<Expression>& base_expression, 
    const vector<std::shared_ptr<Expression>>& parameters
) : MatchedOperatorExpression("(", ")", base_expression, parameters) {

}

FunctionApplication::FunctionApplication(const FunctionApplication& other) : MatchedOperatorExpression(other) {

}

FunctionApplication::~FunctionApplication() {
}

}
}