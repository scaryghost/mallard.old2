#include "mallard/grammar/grouped_expression.hpp"

using std::make_shared;
using std::shared_ptr;
using std::string;

namespace mallard {
namespace grammar {

GroupedExpression::GroupedExpression(const shared_ptr<Expression>& expr) :
    expr(expr)
{

}

GroupedExpression::~GroupedExpression() {
}

void GroupedExpression::flatten(std::ostream& os) const {
    os << "(";
    expr->flatten(os);
    os << ")";
}

shared_ptr<Expression> GroupedExpression::clone() const {
    return make_shared<GroupedExpression>(expr);
}

}
}