#include "mallard/grammar/left_unary_expression.hpp"

using std::make_shared;
using std::shared_ptr;
using std::string;

namespace mallard {
namespace grammar {

LeftUnaryExpression::LeftUnaryExpression(const std::string& operation) : 
    LeftUnaryExpression(shared_ptr<Expression>(), operation)
{
}

LeftUnaryExpression::LeftUnaryExpression(const std::shared_ptr<Expression>& expression, const std::string& operation) :
    expression(expression),
    operation(operation)
{
}

LeftUnaryExpression::~LeftUnaryExpression() {
}

void LeftUnaryExpression::flatten(std::ostream& os) const {
    os << operation << "(";
    expression->flatten(os);
    os << ")";
}

shared_ptr<Expression> LeftUnaryExpression::clone() const {
    return make_shared<LeftUnaryExpression>(expression, operation);
}

}
}