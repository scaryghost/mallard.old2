#include "mallard/grammar/list.hpp"

using std::make_shared;
using std::shared_ptr;
using std::string;
using std::ostream;

namespace mallard {
namespace grammar {

ListExpression::ListExpression(const std::vector<std::shared_ptr<Expression>>& expressions):
    expressions(expressions)
{

}

ListExpression::ListExpression(const ListExpression& other) :
    expressions(other.expressions)
{

}


ListExpression::~ListExpression() {
}

void ListExpression::flatten(ostream& os) const {
    os << "[";

    bool first = true;
    for(auto& e: expressions) {
        if (!first) {
            os << ", ";
        }
        e->flatten(os);
        first = false;
    }

    os << "]";
}

shared_ptr<Expression> ListExpression::clone() const {
    return make_shared<ListExpression>(*this);
}

}
}