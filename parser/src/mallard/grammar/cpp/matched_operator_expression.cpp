#include "mallard/grammar/matched_operator_expression.hpp"

using std::make_shared;
using std::shared_ptr;
using std::string;
using std::ostream;
using std::vector;

namespace mallard {
namespace grammar {

MatchedOperatorExpression::MatchedOperatorExpression(
    const string& open_symbol,
    const string& close_symbol,
    const shared_ptr<Expression>& base_expression, 
    const vector<std::shared_ptr<Expression>>& parameters
) :
    open_symbol(open_symbol),
    close_symbol(close_symbol),
    base_expression(base_expression),
    parameters(parameters)
{

}

MatchedOperatorExpression::MatchedOperatorExpression(const MatchedOperatorExpression& other) :
    open_symbol(open_symbol),
    close_symbol(close_symbol),
    base_expression(base_expression),
    parameters(other.parameters)
{

}


MatchedOperatorExpression::~MatchedOperatorExpression() {
}

void MatchedOperatorExpression::flatten(ostream& os) const {
    base_expression->flatten(os);
    os  << open_symbol;
    
    bool first = true;
    for(auto& p: parameters) {
        if (!first) {
            os << ", ";
        }
        p->flatten(os);
        first = false;
    }

    os << close_symbol;
}

shared_ptr<Expression> MatchedOperatorExpression::clone() const {
    return make_shared<MatchedOperatorExpression>(*this);
}

}
}