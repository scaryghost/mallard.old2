#include "mallard/grammar/operands.hpp"

using std::make_shared;
using std::shared_ptr;
using std::ostream;
using std::string;

namespace mallard {
namespace grammar {

Operand::~Operand() {

}

Identifier::Identifier(const string& name) :
    name(name)
{
}

Identifier::~Identifier() {
}

void Identifier::flatten(ostream& os) const {
    os << name;
}

shared_ptr<Expression> Identifier::clone() const {
    return make_shared<Identifier>(name);
}

Literal::~Literal() {
}

Integer::Integer(const string& value) :
    value(value)
{
}

Integer::~Integer() {
}

void Integer::flatten(ostream& os) const {
    os << value;
}

shared_ptr<Expression> Integer::clone() const {
    return make_shared<Integer>(value);
}

Float::Float(const string& integral, const string& fractional) :
    integral(integral),
    fractional(fractional)
{
}

Float::~Float() {
}

void Float::flatten(ostream& os) const {
    os << integral << "." << fractional;
}

shared_ptr<Expression> Float::clone() const {
    return make_shared<Float>(integral, fractional);
}

String::String(const string& value) :
    value(value)
{
}

String::~String() {
}

void String::flatten(ostream& os) const {
    os << '\"' << value << '\"';
}

shared_ptr<Expression> String::clone() const {
    return make_shared<String>(value);
}

}
}