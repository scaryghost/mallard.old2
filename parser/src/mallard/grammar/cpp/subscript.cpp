#include "mallard/grammar/subscript.hpp"

using std::shared_ptr;
using std::vector;

namespace mallard {
namespace grammar {

Subscript::Subscript(
    const shared_ptr<Expression>& base_expression, 
    const vector<std::shared_ptr<Expression>>& parameters
) : MatchedOperatorExpression("[", "]", base_expression, parameters) {

}

Subscript::Subscript(const Subscript& other) : MatchedOperatorExpression(other) {

}

Subscript::~Subscript() {
}

}
}