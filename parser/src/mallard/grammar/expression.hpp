#pragma once

#include "statement.hpp"

#include <memory>

namespace mallard {
namespace grammar {

struct Expression : public Statement {
    virtual ~Expression() = 0;

    virtual void flatten(std::ostream& os) const = 0;
    virtual std::shared_ptr<Expression> clone() const = 0;
};

}
}