#pragma once

#include "matched_operator_expression.hpp"

namespace mallard {
namespace grammar {

struct FunctionApplication : public MatchedOperatorExpression {
    FunctionApplication(
        const std::shared_ptr<Expression>& base_expression, 
        const std::vector<std::shared_ptr<Expression>>& parameters
    );
    FunctionApplication(const FunctionApplication& other);
    virtual ~FunctionApplication();
};

}
}