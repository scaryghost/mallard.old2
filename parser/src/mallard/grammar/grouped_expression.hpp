#pragma once

#include "expression.hpp"

#include <cstdint>
#include <memory>
#include <string>

namespace mallard {
namespace grammar {

struct GroupedExpression : public Expression {
    GroupedExpression(const std::shared_ptr<Expression>& expr);
    virtual ~GroupedExpression();

    virtual void flatten(std::ostream& os) const;
    virtual std::shared_ptr<Expression> clone() const;

    std::shared_ptr<Expression> expr;
};

}
}