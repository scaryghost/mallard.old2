#pragma once

#include "expression.hpp"

#include <cstdint>
#include <memory>
#include <string>

namespace mallard {
namespace grammar {

struct LeftUnaryExpression : public Expression {
    LeftUnaryExpression(const std::shared_ptr<Expression>& expression, const std::string& operation);
    LeftUnaryExpression(const std::string& operation);
    virtual ~LeftUnaryExpression();

    virtual void flatten(std::ostream& os) const;
    virtual std::shared_ptr<Expression> clone() const;

    std::shared_ptr<Expression> expression;
    std::string operation;
};

}
}