#pragma once

#include "expression.hpp"

#include <cstdint>
#include <memory>
#include <vector>

namespace mallard {
namespace grammar {

struct ListExpression : public Expression {
    ListExpression(const std::vector<std::shared_ptr<Expression>>& expressions);
    ListExpression(const ListExpression& other);
    virtual ~ListExpression();

    virtual void flatten(std::ostream& os) const;
    virtual std::shared_ptr<Expression> clone() const;

    std::vector<std::shared_ptr<Expression>> expressions;
};

}
}