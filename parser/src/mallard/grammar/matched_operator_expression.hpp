#pragma once

#include "expression.hpp"
#include "operands.hpp"

#include <memory>
#include <string>
#include <vector>

namespace mallard {
namespace grammar {

struct MatchedOperatorExpression : public Expression {
    MatchedOperatorExpression(
        const std::string& open_symbol,
        const std::string& close_symbol,
        const std::shared_ptr<Expression>& base_expression, 
        const std::vector<std::shared_ptr<Expression>>& parameters
    );
    MatchedOperatorExpression(const MatchedOperatorExpression& other);
    virtual ~MatchedOperatorExpression();

    virtual void flatten(std::ostream& os) const;
    virtual std::shared_ptr<Expression> clone() const;

    std::string open_symbol, close_symbol;
    std::shared_ptr<Expression> base_expression;
    std::vector<std::shared_ptr<Expression>> parameters;
};

}
}