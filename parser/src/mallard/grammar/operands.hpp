#pragma once

#include "expression.hpp"

#include <string>

namespace mallard {
namespace grammar {

struct Operand : public Expression {
    virtual ~Operand() = 0;

    virtual void flatten(std::ostream& os) const = 0;
    virtual std::shared_ptr<Expression> clone() const = 0;
};

struct Identifier : public Operand {
    Identifier(const std::string& name);
    virtual ~Identifier();

    virtual void flatten(std::ostream& os) const;
    virtual std::shared_ptr<Expression> clone() const;

    std::string name;
};

struct Literal : public Operand {
    virtual ~Literal() = 0;

    virtual void flatten(std::ostream& os) const = 0;
    virtual std::shared_ptr<Expression> clone() const = 0;
};

struct Integer : public Literal {
    Integer(const std::string& value);
    virtual ~Integer();

    virtual void flatten(std::ostream& os) const;
    virtual std::shared_ptr<Expression> clone() const;

    std::string value;
};

struct Float : public Literal {
    Float(const std::string& integral, const std::string& fractional);
    virtual ~Float();

    virtual void flatten(std::ostream& os) const;
    virtual std::shared_ptr<Expression> clone() const;

    std::string integral, fractional;
};

struct String : public Literal {
    String(const std::string& value);
    virtual ~String();

    virtual void flatten(std::ostream& os) const;
    virtual std::shared_ptr<Expression> clone() const;

    std::string value;
};

}
}