#pragma once

#include <ostream>
#include <string>

namespace mallard {
namespace grammar {

struct Statement {
    virtual ~Statement() = 0;

    virtual void flatten(std::ostream& os) const = 0;
};

}
}