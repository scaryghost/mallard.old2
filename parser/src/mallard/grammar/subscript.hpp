#pragma once

#include "matched_operator_expression.hpp"

namespace mallard {
namespace grammar {

struct Subscript : public MatchedOperatorExpression {
    Subscript(
        const std::shared_ptr<Expression>& base_expression, 
        const std::vector<std::shared_ptr<Expression>>& parameters
    );
    Subscript(const Subscript& other);
    virtual ~Subscript();
};

}
}