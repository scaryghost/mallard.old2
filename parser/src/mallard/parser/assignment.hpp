#pragma once

#include "mallard/grammar/assignment.hpp"
#include "mallard/grammar/expression.hpp"

#include "expression.hpp"
#include "parser.hpp"
#include "token.hpp"

#include <memory>
#include <string>
#include <vector>

namespace mallard {
namespace parser {


struct Assignment : public Parser {
    Assignment();
    virtual ~Assignment();

    std::shared_ptr<grammar::Statement> statement() const {
        return std::make_shared<grammar::Assignment>(identifier, std::dynamic_pointer_cast<grammar::Expression>(expression_parser.statement()));
    }
    bool is_accepted() const {
        return state == State::EXPRESSION && expression_parser.is_accepted();
    }

    void munch(const Token& token);
    
private:
    enum class State {
        VAL,
        IDENTIFIER,
        EQUALS,
        EXPRESSION
    };

    std::string identifier;
    Expression expression_parser;
    State state;
};

}
}