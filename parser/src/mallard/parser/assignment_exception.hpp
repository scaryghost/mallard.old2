#pragma once

#include "exception.hpp"

namespace mallard {
namespace parser {

class AssignmentException : public ParserException {
public:
    AssignmentException(const std::string& what_arg);	
    AssignmentException(const char* what_arg);
    AssignmentException(const AssignmentException& other) noexcept;
};

}
}