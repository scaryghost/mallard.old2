#include "mallard/grammar/anonymous_function.hpp"

#include "mallard/parser/exception.hpp"
#include "mallard/parser/statement.hpp"

#include "anonymous_function.hpp"
#include "terminal.hpp"

#include <algorithm>
#include <exception>
#include <sstream>

using namespace std;

namespace mallard {
namespace parser {

AnonymousFunction::AnonymousFunction() : 
    state(State::INIT)
{

}

AnonymousFunction::~AnonymousFunction() {

}

shared_ptr<grammar::Statement> AnonymousFunction::statement() const {
    vector<shared_ptr<grammar::Statement>> statements;
    transform(this->statements.begin(), this->statements.end(), back_inserter(statements), [](const shared_ptr<Parser>& p) {
        return p->statement();
    });

    vector<shared_ptr<grammar::Identifier>> bound_variables;
    transform(this->bound_variables.begin(), this->bound_variables.end(), back_inserter(bound_variables), [](const shared_ptr<Parser>& p) {
        return dynamic_pointer_cast<grammar::Identifier>(p->statement());
    });

    return make_shared<grammar::AnonymousFunction>(statements, bound_variables);
}

bool AnonymousFunction::is_accepted() const {
    return state == State::SINGLE_STATEMENT && statements.back()->is_accepted() || state == State::RIGHT_CURLY_BRACE;
}

void AnonymousFunction::munch(const Token& token) {
    switch(state) {
    case State::INIT:
        if (token.values[0] == "\\") {
            state = State::BOUND_VARIABLE_SLASH;
            bound_variables.push_back(TerminalParser::create_identifier_parser());
            break;
        } else if (token.values[0] == "(") {
            state = State::EMPTY_PARAM_OPEN;
            break;
        }

        throw ParserException("Lambda with bound variables must start with '\\'");
    case State::BOUND_VARIABLE_SLASH: {
        auto last = bound_variables.back();
        try {
            last->munch(token);
            break;
        } catch(const ParserException& e) {
            if (last->is_accepted()) {
                state = State::BOUND_VARIABLE_NAME;
            } else {
                stringstream msg;
                msg << "Invalid tokne after bound variable: " << token;

                throw ParserException(msg.str());
            }
        }
    }
    case State::BOUND_VARIABLE_NAME:
        if (token.values[0] == "\\") {
            state = State::BOUND_VARIABLE_SLASH;
            bound_variables.push_back(TerminalParser::create_identifier_parser());
            break;
        } else if (token.values[0] == "->") {
            state = State::LEFT_CURLY_BRACE;
            break;
        }

        throw ParserException("Expecting '->' or another bound variable");
    case State::EMPTY_PARAM_OPEN:
        if (token.values[0] == ")") {
            state = State::EMPTY_PARAM_CLOSE;
            break;
        }

        throw ParserException("Must close empty bound variables with ')'");
    case State::EMPTY_PARAM_CLOSE:
        if (token.values[0] == "->") {
            state = State::LEFT_CURLY_BRACE;
            break;
        }

        throw ParserException("Must follow bound variables with '->'");
    case State::LEFT_CURLY_BRACE:
        if (token.values[0] == "{") {
            state = State::STATEMENT;
            break;
        }

        statements.push_back(make_shared<Statement>());
        state = State::SINGLE_STATEMENT;
    case State::SINGLE_STATEMENT:
        try {
            statements.back()->munch(token);
            break;
        } catch (const ParserException& e) {
            throw_with_nested(ParserException("Illegal statement inside the anonymous function"));
        }
    case State::STATEMENT:
        if (token.values[0] == "}") {
            if (statements.empty()) {
                state = State::RIGHT_CURLY_BRACE;
                break;
            }

            if (statements.back()->is_accepted()) {
                state = State::RIGHT_CURLY_BRACE;
                break;
            } else {
                throw ParserException("Last statement in anonymous function is not complete");
            }
        } else {
            try {
                if (statements.empty()) {
                    statements.push_back(make_shared<Statement>());
                }
                statements.back()->munch(token);
                break;
            } catch (const ParserException&) {
                if (statements.back()->is_accepted()) {
                    statements.push_back(make_shared<Statement>());
                    munch(token);
                    break;
                } else {
                    throw_with_nested(ParserException("Error parsing last statement in anonymous function"));
                }
            }
        }
    case State::RIGHT_CURLY_BRACE:
        throw ParserException("Anonymous function is already complete");
    }
    
}


}
}