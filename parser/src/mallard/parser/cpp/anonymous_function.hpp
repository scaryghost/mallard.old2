#pragma once

#include "mallard/parser/parser.hpp"

#include <vector>

namespace mallard {
namespace parser {

struct AnonymousFunction : public Parser {
    AnonymousFunction();
    virtual ~AnonymousFunction();

    virtual std::shared_ptr<grammar::Statement> statement() const;
    virtual bool is_accepted() const;

    virtual void munch(const Token& token);

private:
    enum State {
        INIT,
        BOUND_VARIABLE_SLASH,
        BOUND_VARIABLE_NAME,
        EMPTY_PARAM_OPEN,
        EMPTY_PARAM_CLOSE,
        ARROW,
        LEFT_CURLY_BRACE,
        SINGLE_STATEMENT,
        STATEMENT,
        RIGHT_CURLY_BRACE
    };

    std::vector<std::shared_ptr<Parser>> statements, bound_variables;
    State state;
};

}
}