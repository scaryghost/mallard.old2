#include "mallard/parser/assignment.hpp"
#include "mallard/parser/assignment_exception.hpp"

#include <exception>
#include <sstream>

using std::dynamic_pointer_cast;
using std::string;
using std::throw_with_nested;

namespace mallard {
namespace parser {

Assignment::Assignment() : state(State::VAL) {

}

Assignment::~Assignment() {
    
}

void Assignment::munch(const Token& token) {
    switch(state) {
    case State::VAL:
        if (token.type == TokenType::KEYWORD && token.values[0] == "val") {
            state = State::IDENTIFIER;
            return;
        }

        throw AssignmentException("Assignment must start with 'val'");
    case State::IDENTIFIER:
        if (token.type == TokenType::IDENTIFIER) {
            identifier = token.values[0];
            state = State::EQUALS;
            return;
        }

        throw AssignmentException("Assignment must have a valid identifier after 'val'");
    case State::EQUALS:
        if (token.type == TokenType::SYMBOL && token.values[0] == "=") {
            state = State::EXPRESSION;
            return;
        }

        throw AssignmentException("Assignment must follow an identifier with '='");
    case State::EXPRESSION:
        try {
            expression_parser.munch(token);
            break;
        } catch (...) {
            throw_with_nested(AssignmentException("Invalid exception provided"));
        }
    }
}


}
}