#include "mallard/parser/assignment_exception.hpp"

using namespace std;

namespace mallard {
namespace parser {

AssignmentException::AssignmentException(const std::string& what_arg) : ParserException(what_arg) {

}

AssignmentException::AssignmentException(const char* what_arg) : ParserException(what_arg) {

}

AssignmentException::AssignmentException(const AssignmentException& other) noexcept : ParserException(other) {
    
}

}
}