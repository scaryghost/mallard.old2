#include "mallard/grammar/binary_expression.hpp"
#include "mallard/grammar/grouped_expression.hpp"
#include "mallard/grammar/expression.hpp"

#include "mallard/parser/exception.hpp"
#include "mallard/parser/expression.hpp"

#include "binary_expression.hpp"
#include "operators.hpp"

#include <sstream>

using std::dynamic_pointer_cast;
using std::make_shared;
using std::shared_ptr;
using std::stringstream;

namespace mallard {
namespace parser {

BinaryExpressionParser::BinaryExpressionParser() : 
    state(State::LEFT)
{

}

BinaryExpressionParser::BinaryExpressionParser(const shared_ptr<Parser>& left_parser) :
    left_parser(left_parser),
    state(State::LEFT)
{

}

BinaryExpressionParser::~BinaryExpressionParser() {

}


shared_ptr<grammar::Statement> BinaryExpressionParser::statement() const {
    auto left_expr = dynamic_pointer_cast<grammar::Expression>(left_parser->statement());
    auto right_expr = dynamic_pointer_cast<grammar::Expression>(right_parser->statement());
    auto result = make_shared<grammar::BinaryExpression>(left_expr, right_expr, binary_operator, priority);

    if (auto left_binary_expr = dynamic_pointer_cast<grammar::BinaryExpression>(left_expr)) {
        if (left_binary_expr->priority < priority) {
            result->left = left_binary_expr->right;
            left_binary_expr->right = result;
        } else {
            result->left = left_expr;
        }
    } else if (!dynamic_pointer_cast<grammar::GroupedExpression>(left_expr)) {
        if (auto right_binary_expr = dynamic_pointer_cast<grammar::BinaryExpression>(right_expr)) {
            auto current = right_binary_expr->current;
            if (current->priority <= priority) {
                current->current = result;
                result->right = current->left;
                current->left = result;
                return right_binary_expr;
            }
        }
    }

    result->current = result;
    return result;
}

bool BinaryExpressionParser::is_accepted() const {
    return state == State::RIGHT && right_parser->is_accepted();
}

void BinaryExpressionParser::munch(const Token& token) {
    switch(state) {
    case State::LEFT:
        if (!left_parser) {
            left_parser = make_shared<Expression>();
        }

        try {
            left_parser->munch(token);
            break;
        } catch(const ParserException& e) {
            if (left_parser->is_accepted()) {
                state = State::BINARY_OP;
            } else {
                throw e;
            }
        }
    case State::BINARY_OP: {
        auto it = BINARY_OPERATORS.find(token.values[0]);
        if (it != BINARY_OPERATORS.end()) {
            binary_operator = token.values[0];
            priority = it->second;
            state = State::RIGHT;
            right_parser = make_shared<Expression>();
            break;
        }

        stringstream msg;
        msg << token << " is not a binary operator";

        throw ParserException(msg.str());
    }
    case State::RIGHT:
        right_parser->munch(token);
        break;
    }
}


}
}