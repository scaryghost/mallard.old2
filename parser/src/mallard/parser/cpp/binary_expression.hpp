#pragma once

#include "mallard/parser/parser.hpp"

#include <cstddef>

namespace mallard {
namespace parser {

struct BinaryExpressionParser : public Parser {
    BinaryExpressionParser();
    BinaryExpressionParser(const std::shared_ptr<Parser>& left_parser);
    virtual ~BinaryExpressionParser();

    virtual std::shared_ptr<grammar::Statement> statement() const;
    virtual bool is_accepted() const;

    virtual void munch(const Token& token);

private:
    enum State {
        LEFT,
        BINARY_OP,
        RIGHT
    };

    std::shared_ptr<Parser> left_parser, right_parser;
    std::string binary_operator;
    std::uint8_t priority;
    State state;
};

}
}