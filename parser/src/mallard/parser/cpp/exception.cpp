#include "mallard/parser/exception.hpp"

using namespace std;

namespace mallard {
namespace parser {

ParserException::ParserException(const std::string& what_arg) : MallardException(what_arg) {

}

ParserException::ParserException(const char* what_arg) : MallardException(what_arg) {

}

ParserException::ParserException(const ParserException& other) noexcept : MallardException(other) {
    
}

}
}