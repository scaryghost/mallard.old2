#include "mallard/parser/exception.hpp"
#include "mallard/parser/expression.hpp"

#include "binary_expression.hpp"
#include "grouped_expression.hpp"
#include "one_of.hpp"
#include "terminal.hpp"
#include "unary_expression.hpp"


#include <exception>
#include <sstream>

using namespace std;

namespace mallard {
namespace parser {

Expression::Expression() :
    expr_parser(make_shared<UnaryExpressionParser>())
{
    
}

Expression::~Expression() {
    
}

void Expression::munch(const Token& token) {
    try {
        expr_parser->munch(token);
    } catch (const ParserException&) {
        if (dynamic_pointer_cast<OneOfParser>(expr_parser)) {
            auto binary_expr_parser = make_shared<BinaryExpressionParser>(expr_parser);
            binary_expr_parser->munch(token);
            expr_parser = binary_expr_parser;
        } else {
            throw_with_nested(ParserException("No valid expressions satisfy the sequence of tokens"));
        }
    }
}

}
}