#include "mallard/grammar/grouped_expression.hpp"

#include "mallard/parser/exception.hpp"
#include "mallard/parser/expression.hpp"

#include "grouped_expression.hpp"
#include "operators.hpp"

#include <exception>
#include <sstream>

using std::dynamic_pointer_cast;
using std::make_shared;
using std::shared_ptr;
using std::stringstream;
using std::throw_with_nested;

namespace mallard {
namespace parser {

GroupedExpressionParser::GroupedExpressionParser() : 
    state(State::LEFT_PARENS)
{

}

GroupedExpressionParser::~GroupedExpressionParser() {

}

shared_ptr<grammar::Statement> GroupedExpressionParser::statement() const {
    return make_shared<grammar::GroupedExpression>(dynamic_pointer_cast<grammar::Expression>(parser->statement()));
}

bool GroupedExpressionParser::is_accepted() const {
    return state == State::COMPLETED;
}

void GroupedExpressionParser::munch(const Token& token) {
    switch(state) {
    case State::LEFT_PARENS:
        if (token.values[0] == "(") {
            parser = make_shared<Expression>();
            state = State::EXPR;
            break;
        }

        throw ParserException("Grouped expression must start with '('");
    case State::EXPR:
        try {
            parser->munch(token);
            break;
        } catch (const ParserException&) {
            if (parser->is_accepted()) {
                state = State::RIGHT_PARENS;
            } else {
                throw_with_nested(ParserException("Expression in parenthesis is incomplete"));
            }
        } catch (...) {
            throw_with_nested(ParserException("Failed to parse grouped expression"));
        }
    case State::RIGHT_PARENS:
        if (token.values[0] == ")") {
            state = State::COMPLETED;
            break;
        }

        throw ParserException("Grouped expression must end with ')'");
    case State::COMPLETED:
        throw ParserException("Grouped expression is completed");
    }
}


}
}