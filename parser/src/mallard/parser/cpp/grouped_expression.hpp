#pragma once

#include "mallard/parser/parser.hpp"

namespace mallard {
namespace parser {

struct GroupedExpressionParser : public Parser {
    GroupedExpressionParser();
    virtual ~GroupedExpressionParser();

    virtual std::shared_ptr<grammar::Statement> statement() const;
    virtual bool is_accepted() const;

    virtual void munch(const Token& token);

private:
    enum State {
        LEFT_PARENS,
        EXPR,
        RIGHT_PARENS,
        COMPLETED
    };

    std::shared_ptr<Parser> parser;
    State state;
};

}
}