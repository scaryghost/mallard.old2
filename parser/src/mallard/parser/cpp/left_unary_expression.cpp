#include "mallard/grammar/left_unary_expression.hpp"

#include "mallard/parser/exception.hpp"
#include "mallard/parser/expression.hpp"

#include "unary_expression.hpp"
#include "left_unary_expression.hpp"

#include "operators.hpp"

#include <exception>
#include <sstream>

using std::dynamic_pointer_cast;
using std::make_shared;
using std::shared_ptr;
using std::stringstream;
using std::throw_with_nested;

namespace mallard {
namespace parser {

LeftUnaryExpressionParser::LeftUnaryExpressionParser() : 
    state(State::UNARY_OPERATOR)
{

}

LeftUnaryExpressionParser::~LeftUnaryExpressionParser() {

}

shared_ptr<grammar::Statement> LeftUnaryExpressionParser::statement() const {
    return make_shared<grammar::LeftUnaryExpression>(dynamic_pointer_cast<grammar::Expression>(unary_expr_parser->statement()), unary_operator);
}

bool LeftUnaryExpressionParser::is_accepted() const {
    return state == State::EXPR && unary_expr_parser->is_accepted();
}

void LeftUnaryExpressionParser::munch(const Token& token) {
    switch(state) {
    case State::UNARY_OPERATOR: {
        if (LEFT_UNARY_OPERATORS.count(token.values[0])) {
            unary_operator = token.values[0];
            unary_expr_parser = make_shared<UnaryExpressionParser>();
            state = State::EXPR;
            break;
        }

        stringstream msg;
        msg << token << " is not a unary operator";

        throw ParserException(msg.str());
    }
    case State::EXPR:
        unary_expr_parser->munch(token);
        break;    
    }
}


}
}