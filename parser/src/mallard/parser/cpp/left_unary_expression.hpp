#pragma once

#include "mallard/parser/parser.hpp"

#include <string>

namespace mallard {
namespace parser {

struct LeftUnaryExpressionParser : public Parser {
    LeftUnaryExpressionParser();
    virtual ~LeftUnaryExpressionParser();

    virtual std::shared_ptr<grammar::Statement> statement() const;
    virtual bool is_accepted() const;

    virtual void munch(const Token& token);

private:
    enum State {
        UNARY_OPERATOR,
        EXPR
    };

    std::shared_ptr<Parser> unary_expr_parser;
    std::string unary_operator;
    State state;
};

}
}