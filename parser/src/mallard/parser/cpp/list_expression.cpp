#include "mallard/grammar/list.hpp"

#include "mallard/parser/exception.hpp"
#include "mallard/parser/expression.hpp"

#include "list_expression.hpp"

#include <algorithm>
#include <exception>
#include <iterator>
#include <sstream>

using namespace std;

namespace mallard {
namespace parser {

ListExpression::ListExpression() :
    state(State::LEFT_BRACKET)
{
    
}

ListExpression::~ListExpression() {
    
}

shared_ptr<grammar::Statement> ListExpression::statement() const {
    vector<shared_ptr<grammar::Expression>> expressions;
    transform(parsers.begin(), parsers.end(), back_inserter(expressions), [](const shared_ptr<Parser>& p) {
        return dynamic_pointer_cast<grammar::Expression>(p->statement());
    });

    return make_shared<grammar::ListExpression>(expressions);
}

bool ListExpression::is_accepted() const {
    return state == State::RIGHT_BRACKET;
}

void ListExpression::munch(const Token& token) {
    switch(state) {
    case State::LEFT_BRACKET:
        if (token.values[0] == "[") {
            state = State::BEGIN;
            break;
        }

        throw ParserException("List must begin with [");
    case State::BEGIN:
        if (token.values[0] == "]") {
            state = State::RIGHT_BRACKET;
            break;
        } else {
            state = State::EXPR;
            parsers.push_back(make_shared<Expression>());
        }
    case State::EXPR: {
        auto last = parsers.back();
        auto try_munch = [this, &token, &last]() {
            try {
                last->munch(token);
            } catch(const ParserException&) {
                stringstream msg;
                msg << "Expression at index " << parsers.size() - 1 << " is not valie";
                throw_with_nested(ParserException(msg.str()));
            }
        };

        if (token.values[0] == ",") {    
            if (last->is_accepted()) {
                parsers.push_back(make_shared<Expression>());
            } else {
                try_munch();
            }
        } else if (token.values[0] == "]") {
            if (last->is_accepted()) {
                state = State::RIGHT_BRACKET;
            } else {
                try_munch();
            }
        } else {
            last->munch(token);
        }
        break;
    }
    case State::RIGHT_BRACKET:
        throw ParserException("List expression is complete");
    }
}

}
}