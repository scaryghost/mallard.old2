#pragma once

#include "mallard/parser/parser.hpp"

#include <initializer_list>
#include <vector>

namespace mallard {
namespace parser {

struct ListExpression : public Parser {
    ListExpression();
    virtual ~ListExpression();

    virtual std::shared_ptr<grammar::Statement> statement() const;
    virtual bool is_accepted() const;

    virtual void munch(const Token& token);
    
private:
    enum State {
        LEFT_BRACKET,
        BEGIN,
        EXPR,
        RIGHT_BRACKET
    };

    State state;
    std::vector<std::shared_ptr<Parser>> parsers;
};

}
}