#include "mallard/grammar/function_application.hpp"
#include "mallard/grammar/subscript.hpp"

#include "mallard/parser/exception.hpp"
#include "mallard/parser/expression.hpp"

#include "matched_operator_expression.hpp"
#include "grouped_expression.hpp"
#include "one_of.hpp"
#include "terminal.hpp"

#include <algorithm>
#include <exception>
#include <iterator>
#include <sstream>

using namespace std;

namespace mallard {
namespace parser {

const string FN_EXPR_TYPE = "function application";
const string SUBSCRIPT_EXPR_TYPE = "subscript";

shared_ptr<Parser> MatchedOperatorExpression::create_function_application_parser() {
    return make_shared<MatchedOperatorExpression>(FN_EXPR_TYPE, "(", ")");
}

shared_ptr<Parser> MatchedOperatorExpression::create_subscript_parser() {
    return make_shared<MatchedOperatorExpression>(SUBSCRIPT_EXPR_TYPE, "[", "]");
}

MatchedOperatorExpression::MatchedOperatorExpression(const string& expr_type, const string& open_symbol, const string& close_symbol) : 
    base_expression(make_shared<OneOfParser>(initializer_list<shared_ptr<Parser>>{
        TerminalParser::create_identifier_parser(),
        make_shared<GroupedExpressionParser>()
    })),
    expr_type(expr_type),
    open_symbol(open_symbol),
    close_symbol(close_symbol),
    state(State::BASE_EXPR)
{

}

MatchedOperatorExpression::MatchedOperatorExpression(const std::shared_ptr<Parser>& base_expression) :
    base_expression(base_expression)
{

}

MatchedOperatorExpression::~MatchedOperatorExpression() {

}

shared_ptr<grammar::Statement> MatchedOperatorExpression::statement() const {
    vector<shared_ptr<grammar::Expression>> parameters;
    transform(this->parameters.begin(), this->parameters.end(), back_inserter(parameters), [](const shared_ptr<Parser>& it) {
        return dynamic_pointer_cast<grammar::Expression>(it->statement());
    });

    if (expr_type == FN_EXPR_TYPE) {
        return make_shared<grammar::FunctionApplication>(
            dynamic_pointer_cast<grammar::Expression>(base_expression->statement()), 
            parameters
        );
    }
    return make_shared<grammar::Subscript>(
        dynamic_pointer_cast<grammar::Expression>(base_expression->statement()), 
        parameters
    );
}

bool MatchedOperatorExpression::is_accepted() const {
    return state == State::CLOSE_SYMBOL;
}

void MatchedOperatorExpression::munch(const Token& token) {
    switch(state) {
    case State::BASE_EXPR:
        try {
            base_expression->munch(token);
            break;
        } catch(const ParserException& e) {
            if (base_expression->is_accepted()) {
                state = State::OPEN_SYMBOL;
            } else {
                throw_with_nested(ParserException("Invalid base expression provided"));
            }
        }
    case State::OPEN_SYMBOL: {
        if (token.values[0] == open_symbol) {
            state = State::NONE_OR_SOME;
            break;
        }

        stringstream msg;
        msg << "Expecting '" << open_symbol << "' after the base expression";

        throw ParserException(msg.str());
    }
    case State::NONE_OR_SOME:
        if (token.values[0] == close_symbol) {
            state = State::CLOSE_SYMBOL;
            break;
        }

        parameters.push_back(make_shared<Expression>());
        state = State::PARAM_EXPR;
    case State::PARAM_EXPR: {
        auto last = parameters.back();
        try {
            last->munch(token);
        } catch (const ParserException& e) {
            if (last->is_accepted()) {
                if (token.values[0] == ",") {
                    parameters.push_back(make_shared<Expression>());
                } else if (token.values[0] == close_symbol) {
                    state = State::CLOSE_SYMBOL;
                } else {
                    stringstream msg;
                    msg << "Unregconized token after a complete parameter: " << token;

                    throw_with_nested(ParserException(msg.str()));
                }
            } else {
                stringstream msg;
                msg << "Error parsing " << expr_type << " parameter";

                throw_with_nested(ParserException(msg.str()));
            }
        }
        break;
    }
    case State::CLOSE_SYMBOL: {
        if (token.values[0] == open_symbol) {
            base_expression = make_shared<MatchedOperatorExpression>(*this);
            parameters.clear();
            state = State::NONE_OR_SOME;
            break;
        }

        stringstream msg;
        msg << expr_type << " is completed";

        throw ParserException(msg.str());
    }
    }
}


}
}