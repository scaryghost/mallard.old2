#pragma once

#include "mallard/parser/parser.hpp"

#include <string>
#include <vector>

namespace mallard {
namespace parser {

struct MatchedOperatorExpression : public Parser {
    MatchedOperatorExpression(const std::string& expr_type, const std::string& open_symbol, const std::string& close_symbol);
    MatchedOperatorExpression(const std::shared_ptr<Parser>& base_expression);
    virtual ~MatchedOperatorExpression();

    virtual std::shared_ptr<grammar::Statement> statement() const;
    virtual bool is_accepted() const;

    virtual void munch(const Token& token);

    static std::shared_ptr<Parser> create_function_application_parser();
    static std::shared_ptr<Parser> create_subscript_parser();
private:
    enum State {
        BASE_EXPR,
        OPEN_SYMBOL,
        NONE_OR_SOME,
        PARAM_EXPR,
        CLOSE_SYMBOL
    };

    std::vector<std::shared_ptr<Parser>> parameters;
    std::shared_ptr<Parser> base_expression;
    std::string expr_type, open_symbol, close_symbol;
    State state;
};

}
}