#include "mallard/parser/no_such_element.hpp"

using namespace std;

namespace mallard {
namespace parser {

NoSuchElementError::NoSuchElementError(const std::string& what_arg) : MallardException(what_arg) {

}

NoSuchElementError::NoSuchElementError(const char* what_arg) : MallardException(what_arg) {

}

NoSuchElementError::NoSuchElementError(const NoSuchElementError& other) noexcept : MallardException(other) {
    
}

}
}