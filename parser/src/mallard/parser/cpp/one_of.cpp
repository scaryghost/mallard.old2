#include "mallard/parser/exception.hpp"
#include "one_of.hpp"

#include <algorithm>
#include <iterator>
#include <sstream>

using namespace std;

namespace mallard {
namespace parser {

OneOfParser::OneOfParser(const initializer_list<shared_ptr<Parser>>& parsers) :
    parsers(parsers)
{
    
}

OneOfParser::~OneOfParser() {
    
}

shared_ptr<grammar::Statement> OneOfParser::statement() const {
    return find_if(parsers.begin(), parsers.end(), [](const shared_ptr<Parser>& it) {
        return it->is_accepted();
    })->get()->statement();
}

bool OneOfParser::is_accepted() const {
    vector<shared_ptr<Parser>> valid;

    copy_if(parsers.begin(), parsers.end(), back_inserter(valid), [](const shared_ptr<Parser>& it) {
        return it->is_accepted();
    });

    return valid.size() == 1;
}

void OneOfParser::munch(const Token& token) {
    vector<shared_ptr<Parser>> valid;

    copy_if(parsers.begin(), parsers.end(), back_inserter(valid), [&token](const shared_ptr<Parser>& it) {
        try {
            it->munch(token);
            return true;
        } catch(const ParserException& e) {
            return false;
        }
    });

    if (valid.empty()) {
        throw ParserException("No valid parsers for the expression");
    }

    parsers = valid;
}

}
}