#pragma once

#include "mallard/parser/parser.hpp"

#include <initializer_list>
#include <vector>

namespace mallard {
namespace parser {

struct OneOfParser : public Parser {
    OneOfParser(const std::initializer_list<std::shared_ptr<Parser>>& parsers);
    virtual ~OneOfParser();

    virtual std::shared_ptr<grammar::Statement> statement() const;
    virtual bool is_accepted() const;

    virtual void munch(const Token& token);
    
private:
    std::vector<std::shared_ptr<Parser>> parsers;
};

}
}