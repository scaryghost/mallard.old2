#include "operators.hpp"

using std::string;
using std::unordered_map;
using std::unordered_set;
using std::uint8_t;

namespace mallard {
namespace parser {

const std::unordered_set<string> LEFT_UNARY_OPERATORS = {"-", "not", "~"};
const std::unordered_map<string, uint8_t> BINARY_OPERATORS = {
    {".", 12},
    {"**", 11},
    {"*", 10}, {"/", 10}, {"%", 10},
    {"+", 9}, {"-", 9}, 
    {"<<", 8}, {">>", 8}, 
    {"<=>", 8}, 
    {">", 7}, {"<", 7}, {">=", 7}, {"<=", 7},
    {"==", 6}, {"!=", 6},
    {"&", 5},
    {"^", 4},
    {"|", 3},
    {"and", 2},
    {"xor", 1},
    {"or", 0}
};

}
}