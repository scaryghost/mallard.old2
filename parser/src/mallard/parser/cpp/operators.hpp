#pragma once

#include <cstdint>
#include <string>
#include <unordered_map>
#include <unordered_set>

namespace mallard {
namespace parser {

extern const std::unordered_set<std::string> LEFT_UNARY_OPERATORS;
extern const std::unordered_map<std::string, std::uint8_t> BINARY_OPERATORS;

}
}