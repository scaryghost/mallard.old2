#include "mallard/parser/assignment.hpp"
#include "mallard/parser/expression.hpp"
#include "mallard/parser/statement.hpp"

#include "one_of.hpp"

#include <initializer_list>

using namespace std;

namespace mallard {
namespace parser {

Statement::Statement() :
    statement_parser(make_shared<OneOfParser>(initializer_list<shared_ptr<Parser>>{
        make_shared<Assignment>(),
        make_shared<Expression>()
    }))
{
    
}

Statement::~Statement() {
    
}

void Statement::munch(const Token& token) {
    statement_parser->munch(token);
}

shared_ptr<grammar::Statement> Statement::statement() const {
    return statement_parser->statement();
}

bool Statement::is_accepted() const {
    return statement_parser->is_accepted();
}

}
}