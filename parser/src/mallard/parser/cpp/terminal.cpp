#include "mallard/parser/exception.hpp"

#include "terminal.hpp"

#include <sstream>

using std::make_shared;
using std::shared_ptr;
using std::stringstream;

namespace mallard {
namespace parser {

TerminalParser::TerminalParser(TokenType token_type, StatementCreator token_to_statement) :
    token_type(token_type), token_to_statement(token_to_statement)
{

}

TerminalParser::~TerminalParser() {

}

void TerminalParser::munch(const Token& token) {
    if (token.type == token_type) {
        if (!is_accepted()) {
            _statement = token_to_statement(token);
        } else {
            throw ParserException("Parser has already accepted a token");;
        }
    } else {
        stringstream buffer;
        buffer << token << " is not a terminal";

        throw ParserException(buffer.str());
    }
}

}
}