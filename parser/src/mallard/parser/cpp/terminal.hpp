#pragma once

#include "mallard/parser/parser.hpp"

namespace mallard {
namespace parser {

using StatementCreator = std::shared_ptr<grammar::Statement>(*)(const Token& token);

struct TerminalParser : public Parser {
    static std::shared_ptr<Parser> create_int_literal_parser();
    static std::shared_ptr<Parser> create_float_literal_parser();
    static std::shared_ptr<Parser> create_identifier_parser();

    TerminalParser(TokenType token_type, StatementCreator token_to_statement);
    virtual ~TerminalParser();

    virtual std::shared_ptr<grammar::Statement> statement() const {
        return _statement;
    }
    virtual bool is_accepted() const {
        return _statement != nullptr;
    }

    virtual void munch(const Token& token);

private:
    std::shared_ptr<grammar::Statement> _statement;
    StatementCreator token_to_statement;
    TokenType token_type;
};

}
}