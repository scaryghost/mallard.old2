#include "mallard/grammar/operands.hpp"

#include "terminal.hpp"

using std::make_shared;
using std::shared_ptr;

namespace mallard {
namespace parser {

shared_ptr<Parser> TerminalParser::create_int_literal_parser() {
    return make_shared<TerminalParser>(
        TokenType::INT_LITERAL, 
        [](const Token& token) -> shared_ptr<grammar::Statement> {
            return make_shared<grammar::Integer>(token.values[0]);
        }
    );
}

shared_ptr<Parser> TerminalParser::create_float_literal_parser() {
    return make_shared<TerminalParser>(
        TokenType::FLOAT_LITERAL, 
        [](const Token& token) -> shared_ptr<grammar::Statement> {
            return make_shared<grammar::Float>(token.values[0], token.values[1]);
        }
    );
}

shared_ptr<Parser> TerminalParser::create_identifier_parser() {
    return make_shared<TerminalParser>(
        TokenType::IDENTIFIER, 
        [](const Token& token) -> shared_ptr<grammar::Statement> {
            return make_shared<grammar::Identifier>(token.values[0]);
        }
    );
}

}
}