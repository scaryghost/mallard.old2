#include "mallard/parser/no_such_element.hpp"
#include "mallard/parser/tokenizer.hpp"

#include "mallard/recognizer/exception.hpp"
#include "mallard/recognizer/one_of.hpp"
#include "mallard/recognizer/recognizers.hpp"

#include <cstddef>
#include <cstdint>
#include <sstream>
#include <unordered_map>

using namespace std;
using namespace mallard::recognizer;

namespace mallard {
namespace parser {

bool operator==(const Token& self, const Token& other) {
    return self.values == other.values && self.type == other.type;
}

ostream& operator <<(ostream& os, const Token& self) {
    os << "Token(values=[";

    bool first = true;
    for(auto& it: self.values) {
        if (!first) {
            os << ", ";
        }

        os << it;
        first = false;
    }
    os << "], type=" << static_cast<int32_t>(self.type) << ")";

    return os;
}

Tokenizer::Tokenizer(istream& is) : is(is), has_last_char(false) {
    is.exceptions(istream::failbit | istream::eofbit | istream::badbit);
    count = 0;
}

Tokenizer& Tokenizer::operator>>(Token& token) {
    Identifier identifier_recognizer;
    DFARecognizer symbol_recognizer = symbol(), keyword_recognizer = keyword();
    StringLiteral string_recognizer;
    FloatLiteral float_recognizer;
    IntLiteral int_recognizer;
    OneOf recognizers({&identifier_recognizer, &symbol_recognizer, &string_recognizer, &keyword_recognizer, &float_recognizer, &int_recognizer});
    unordered_map<Recognizer*, TokenType> token_types = {
        {&identifier_recognizer, TokenType::IDENTIFIER},
        {&symbol_recognizer, TokenType::SYMBOL},
        {&string_recognizer, TokenType::STRING_LITERAL},
        {&float_recognizer, TokenType::FLOAT_LITERAL},
        {&keyword_recognizer, TokenType::KEYWORD},
        {&int_recognizer, TokenType::INT_LITERAL}
    };

    char ch;
    bool munched = false;
    stringstream buffer;

    while(true) {
        try {
            ch = next_char();
            if (munched || !isspace(ch)) {
                munched = true;

                buffer << ch;
                recognizers.munch(ch);
            }
        } catch (const RecognizerException& e) {
            if (recognizers.has_accepted()) {
                last_char = ch;
                has_last_char = true;
                break;
            }

            throw e;
        } catch(...) {
            break;
        }

    }

    if (munched) {
        auto accepted = recognizers.accepted_recognizers();
        if (accepted.size() != 1) {
            stringstream os;

            os << "'" << buffer.str() << "' is a not a valid token";
            throw RecognizerException(os.str());
        }

        token.type = token_types.at(accepted.front());
        token.values = recognizers.matches();
    } else {
        throw NoSuchElementError("No valid characters were pulled from the stream");
    }

    return *this;
}

char Tokenizer::next_char() {
    count++;
    char value;
    if (has_last_char) {
        value = last_char;
        has_last_char = false;
    } else {
        value = is.get();
    }
    return value;
}

}
}