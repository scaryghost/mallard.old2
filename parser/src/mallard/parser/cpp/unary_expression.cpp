#include "anonymous_function.hpp"
#include "grouped_expression.hpp"
#include "left_unary_expression.hpp"
#include "list_expression.hpp"
#include "matched_operator_expression.hpp"
#include "terminal.hpp"
#include "unary_expression.hpp"

#include <initializer_list>

using std::make_shared;
using std::initializer_list;
using std::shared_ptr;

namespace mallard {
namespace parser {

UnaryExpressionParser::UnaryExpressionParser() : 
    OneOfParser(initializer_list<shared_ptr<Parser>>{
        TerminalParser::create_identifier_parser(),
        TerminalParser::create_float_literal_parser(),
        TerminalParser::create_int_literal_parser(),
        make_shared<LeftUnaryExpressionParser>(),
        make_shared<GroupedExpressionParser>(),
        make_shared<ListExpression>(),
        make_shared<AnonymousFunction>(),
        MatchedOperatorExpression::create_function_application_parser(),
        MatchedOperatorExpression::create_subscript_parser()
    })
{

}

UnaryExpressionParser::~UnaryExpressionParser() {

}

}
}