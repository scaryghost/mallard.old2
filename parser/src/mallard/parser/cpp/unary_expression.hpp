#pragma once

#include "one_of.hpp"

#include <string>

namespace mallard {
namespace parser {

struct UnaryExpressionParser : public OneOfParser {
    UnaryExpressionParser();
    virtual ~UnaryExpressionParser();
};

}
}