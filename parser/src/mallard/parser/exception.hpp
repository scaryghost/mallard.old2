#pragma once

#include "mallard/exception/mallard_exception.hpp"

namespace mallard {
namespace parser {

class ParserException : public exception::MallardException {
public:
    ParserException(const std::string& what_arg);	
    ParserException(const char* what_arg);
    ParserException(const ParserException& other) noexcept;
};

}
}