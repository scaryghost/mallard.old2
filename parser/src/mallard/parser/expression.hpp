#pragma once

#include "parser.hpp"

namespace mallard {
namespace parser {

struct Expression : public Parser {
    Expression();
    virtual ~Expression();

    virtual std::shared_ptr<grammar::Statement> statement() const {
        return expr_parser->statement();
    }
    virtual bool is_accepted() const {
        return expr_parser->is_accepted();
    }

    virtual void munch(const Token& token);
    
private:
    std::shared_ptr<Parser> expr_parser;
};

}
}