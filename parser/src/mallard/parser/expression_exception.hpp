#pragma once

#include "exception.hpp"

namespace mallard {
namespace parser {

class ExpressionException : public ParserException {
public:
    ExpressionException(const std::string& what_arg);	
    ExpressionException(const char* what_arg);
    ExpressionException(const ExpressionException& other) noexcept;
};

}
}