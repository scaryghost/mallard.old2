#pragma once

#include "mallard/exception/mallard_exception.hpp"

namespace mallard {
namespace parser {

class NoSuchElementError : public exception::MallardException {
public:
    NoSuchElementError(const std::string& what_arg);	
    NoSuchElementError(const char* what_arg);
    NoSuchElementError(const NoSuchElementError& other) noexcept;
};

}
}