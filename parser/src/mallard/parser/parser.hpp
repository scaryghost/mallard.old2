#pragma once

#include "mallard/grammar/statement.hpp"
#include "token.hpp"

#include <memory>

namespace mallard {
namespace parser {

struct Parser {
    virtual ~Parser() = 0;

    virtual std::shared_ptr<grammar::Statement> statement() const = 0;
    virtual bool is_accepted() const = 0;

    virtual void munch(const Token& token) = 0;
};

}
}