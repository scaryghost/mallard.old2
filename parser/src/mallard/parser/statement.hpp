#pragma once

#include "parser.hpp"

namespace mallard {
namespace parser {

struct Statement : public Parser {
    Statement();
    virtual ~Statement();

    virtual std::shared_ptr<grammar::Statement> statement() const;
    virtual bool is_accepted() const;

    virtual void munch(const Token& token);
    
private:
    std::shared_ptr<Parser> statement_parser;
};

}
}