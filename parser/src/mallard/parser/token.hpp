#pragma once

#include <ostream>
#include <string>
#include <vector>

namespace mallard {
namespace parser {

enum class TokenType {
    INT_LITERAL,
    FLOAT_LITERAL,
    IDENTIFIER,
    KEYWORD,
    STRING_LITERAL,
    SYMBOL
};

struct Token {
    std::vector<std::string> values;
    TokenType type;
};

bool operator==(const Token& self, const Token& other);
std::ostream& operator <<(std::ostream& os, const Token& self);

}
}