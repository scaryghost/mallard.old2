#pragma once

#include "mallard/parser/token.hpp"

#include <istream>
#include <string>
#include <vector>

namespace mallard {
namespace parser {

bool operator==(const Token& self, const Token& other);

struct Tokenizer {
    Tokenizer(std::istream& is);

    Tokenizer& operator>>(Token& token);

private:
    int count;
    char next_char(); 

    std::istream& is;
    bool has_last_char;
    char last_char;
};

}
}