#include "mallard/reader/stream.hpp"
#include "mallard/parser/exception.hpp"
#include "mallard/parser/statement.hpp"

#include <exception>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

namespace mallard {
namespace reader {

StatementStream::StatementStream(istream& is) : 
    tokenizer(is), 
    has_last_token(false),
    _has_next(true)
{
}

StatementStream& StatementStream::operator >>(shared_ptr<grammar::Statement>& statement) {
    parser::Statement parser;
    auto assign_token = [&parser, &statement, this](const parser::Token& token) {
        last_token = token;
        has_last_token = true;
        statement = parser.statement();
    };

    while(true) {
        parser::Token token;
        string error_msg;

        try {
            token = next_token();
            parser.munch(token);
        } catch(const parser::ParserException& e) {
            if (parser.is_accepted()) {
                assign_token(token);
                return *this;
            }

            throw_with_nested(parser::ParserException("Invalid statement provided"));
        } catch(...) {
            _has_next = false;

            if (parser.is_accepted()) {
                assign_token(token);
                return *this;
            }

            throw_with_nested(parser::ParserException("Incomplete statement provided, unable to read further from the stream"));
        }
    }

    return *this;
}

parser::Token StatementStream::next_token() {
    parser::Token token;
    if (has_last_token) {
        token = last_token;
        has_last_token = false;
    } else {
        tokenizer >> token;
    }
    return token;
}

}
}