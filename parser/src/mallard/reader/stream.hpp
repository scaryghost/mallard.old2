#pragma once

#include "mallard/grammar/statement.hpp"
#include "mallard/parser/tokenizer.hpp"

#include <istream>
#include <memory>

namespace mallard {
namespace reader {

struct StatementStream {
    StatementStream(std::istream& is);

    StatementStream& operator >>(std::shared_ptr<grammar::Statement>& statement);
    bool has_next() const {
        return _has_next;
    }
private:
    parser::Tokenizer tokenizer;
    parser::Token last_token;
    bool has_last_token, _has_next;

    parser::Token next_token();
};

}
}