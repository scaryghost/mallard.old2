#include "mallard/recognizer/dfa_recognizer.hpp"
#include "mallard/recognizer/exception.hpp"

#include <algorithm>
#include <iterator>

using std::back_inserter;
using std::move;
using std::string;
using std::stringstream;
using std::transform;
using std::vector;

namespace mallard {
namespace recognizer {

DFARecognizer::DFARecognizer(std::size_t n_matches, const vector<State>& states) : 
    buffers(n_matches),
    states(states),
    current(0)
{

}

DFARecognizer::DFARecognizer(DFARecognizer&& other) : 
    buffers(move(other.buffers)),
    states(move(other.states)),
    current(move(other.current))
{

}

DFARecognizer::~DFARecognizer() {

}

void DFARecognizer::munch(char ch) {
    auto state = states[current];
    auto it = state.transitions.find(ch);

    if (it == state.transitions.end()) {
        stringstream os;
        os << "'" << buffers[0].str() << ch << "' is not a valid token";

        throw RecognizerException(os.str());
    }

    buffers[0] << ch;
    current = it->second;
}

vector<string> DFARecognizer::matches() const {
    vector<string> results;
    transform(buffers.begin(), buffers.end(), back_inserter(results), [](const stringstream& e) {
        return e.str();
    });

    return results;
}

}
}