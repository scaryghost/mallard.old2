#include "mallard/recognizer/exception.hpp"

using namespace std;

namespace mallard {
namespace recognizer {

RecognizerException::RecognizerException(const std::string& what_arg) : MallardException(what_arg) {

}

RecognizerException::RecognizerException(const char* what_arg) : MallardException(what_arg) {

}

RecognizerException::RecognizerException(const RecognizerException& other) noexcept : MallardException(other) {
    
}

}
}