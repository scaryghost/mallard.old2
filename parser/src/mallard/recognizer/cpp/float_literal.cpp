#include "mallard/recognizer/float_literal.hpp"
#include "mallard/recognizer/exception.hpp"

#include <cctype>
#include <sstream>
#include <stdexcept>

using namespace std;

namespace mallard {
namespace recognizer {

FloatLiteral::FloatLiteral() :  
    intergralRecognizer(int_decimal_literal()), 
    fractionalRecognizer(int_decimal_literal()),
    _state(State::INTERGRAL)
{

}

FloatLiteral::~FloatLiteral() {

}

void FloatLiteral::munch(char ch) {
    switch(_state) {
    case State::INTERGRAL:
        try {
            intergralRecognizer.munch(ch);
        } catch (const RecognizerException&) {
            if (intergralRecognizer.has_accepted() && ch == '.') {
                _state = FRACTIONAL;
            } else {
                stringstream os;

                os << "'" << intergralRecognizer.matches()[0] << ch << "' is not a valid float";
                throw RecognizerException(os.str());
            }
        }
        break;
    case State::FRACTIONAL:
        try {
            fractionalRecognizer.munch(ch);
        } catch (...) {
            stringstream os;

            os << "'" << intergralRecognizer.matches()[0] << '.' << fractionalRecognizer.matches()[0] << "' is not a valid float";
            throw RecognizerException(os.str());
        }
        break;
    }
}

}
}