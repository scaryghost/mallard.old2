#include "mallard/recognizer/identifier.hpp"
#include "mallard/recognizer/keyword.hpp"

#include "transitions.hpp"

using std::string;
using std::vector;

namespace mallard {
namespace recognizer {

static const vector<State> STATES = {
    {false, {{'_', 1}, ALPHA_LOWER(1), ALPHA_UPPER(1)}},
    {true, {{'_', 1}, ALPHA_LOWER(1), ALPHA_UPPER(1), DIGITS(1)}}
};

Identifier::Identifier() : 
    OneButNot(&identifier_recognizer, {&keyword_recognizer}),
    keyword_recognizer(keyword()),
    identifier_recognizer(1, STATES)
{

}

Identifier::~Identifier() {

}

}
}