#include "mallard/recognizer/int_literal.hpp"
#include "transitions.hpp"

#include <cctype>
#include <sstream>
#include <stdexcept>

using namespace std;

namespace mallard {
namespace recognizer {

static const vector<State> BINARY_STATES = {
    {false, {{'0', 1}, {'1', 1}}},
    {false, {{'0', 1}, {'1', 1}, {'b', 2}}},
    {true, {}}
}, DECIMAL_STATES = {
    {false, {DIGITS(1)}},
    {true, {DIGITS(1)}}
}, HEX_STATES = {
    {false, {{'0', 1}}},
    {false, {{'x', 2}}},
    {false, {DIGITS(3), HEX_DIGITS(3)}},
    {true, {DIGITS(3), HEX_DIGITS(3)}}
}, OCTAL_STATES = {
    {false, {{'0', 1}, {'1', 1}, {'2', 1}, {'3', 1}, {'4', 1}, {'5', 1}, {'6', 1}, {'7', 1}}},
    {false, {{'0', 1}, {'1', 1}, {'2', 1}, {'3', 1}, {'4', 1}, {'5', 1}, {'6', 1}, {'7', 1}, {'o', 2}}},
    {true, {}}
};

IntLiteral::IntLiteral() : 
    OneOf({&binaryRecognizer, &decimalRecognizer, &hexRecognizer, &octRecognizer}),
    binaryRecognizer(1, BINARY_STATES), 
    decimalRecognizer(1, DECIMAL_STATES), 
    hexRecognizer(1, HEX_STATES), 
    octRecognizer(1, OCTAL_STATES)
{

}

IntLiteral::~IntLiteral() {

}

DFARecognizer int_binary_literal() {
    return DFARecognizer(1, BINARY_STATES);
}

DFARecognizer int_decimal_literal() {
    return DFARecognizer(1, DECIMAL_STATES);
}

DFARecognizer int_hex_literal() {
    return DFARecognizer(1, HEX_STATES);
}

DFARecognizer int_oct_literal() {
    return DFARecognizer(1, OCTAL_STATES);
}

}
}