#include "mallard/recognizer/keyword.hpp"

#include <vector>

using std::vector;

namespace mallard {
namespace recognizer {

DFARecognizer keyword() {
    static const vector<State> STATES = {
        {false, {{'v', 2}}},
        {true, {}},
        {false, {{'a', 3}}},
        {false, {{'l', 1}}}
    };

    return DFARecognizer(1, STATES);
}

}
}