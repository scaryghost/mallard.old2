#include "mallard/recognizer/one_but_not.hpp"
#include "mallard/recognizer/exception.hpp"

#include <algorithm>
#include <iterator>
#include <sstream>

using namespace std;

namespace mallard {
namespace recognizer {

OneButNot::OneButNot(Recognizer* target, const std::initializer_list<Recognizer*>& others) : 
    others(others),
    target(target),
    others_rejected(false)
{

}

OneButNot::~OneButNot() {

}

void OneButNot::munch(char ch) {
    target->munch(ch);

    if (!others_rejected) {
        try {
            others.munch(ch);
        } catch (const RecognizerException&) {
            others_rejected = true;
        }
    }
}

}
}