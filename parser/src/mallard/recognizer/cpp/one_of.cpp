#include "mallard/recognizer/one_of.hpp"
#include "mallard/recognizer/exception.hpp"

#include <algorithm>
#include <iterator>
#include <sstream>

using namespace std;

namespace mallard {
namespace recognizer {

OneOf::OneOf(const initializer_list<Recognizer*>& recognizers) : _recognizers(recognizers) {

}

OneOf::~OneOf() {

}

void OneOf::munch(char ch) {
    vector<Recognizer*> still_valid;

    copy_if(_recognizers.begin(), _recognizers.end(), back_inserter(still_valid), [ch](Recognizer* e) {
        try {
            e->munch(ch);
            return true;
        } catch(const RecognizerException&) {
            return false;
        }
    });

    if (still_valid.empty()) {
        stringstream os;
        os << "Invalid character given: '" << ch << "'";

        throw RecognizerException(os.str());
    }

    _recognizers = still_valid;
}

vector<Recognizer*> OneOf::accepted_recognizers() const {
    vector<Recognizer*> accepted;

    copy_if(_recognizers.begin(), _recognizers.end(), back_inserter(accepted), [](const Recognizer* e) {
        return e->has_accepted();
    });

    return accepted;
}

}
}