
#include "mallard/recognizer/string_literal.hpp"
#include "mallard/recognizer/exception.hpp"

#include <cctype>
#include <sstream>
#include <stdexcept>

using namespace std;

namespace mallard {
namespace recognizer {

StringLiteral::StringLiteral() : 
    state(State::EMPTY)
{

}

StringLiteral::~StringLiteral() {

}

void StringLiteral::munch(char ch) {
    switch(state) {
    case State::EMPTY:
        if (ch == '\"') {
            state = State::READING_CHARS;
        } else {
            throw RecognizerException("String literal must begin with double quote");
        }
        break;
    case State::READING_CHARS:
        if (ch == '\"') {
            state = State::END_STR;
        } else {
            buffer << ch;
        }
        break;
    case State::END_STR:
        throw RecognizerException("String literal already terminated");
    }
}

}   
}