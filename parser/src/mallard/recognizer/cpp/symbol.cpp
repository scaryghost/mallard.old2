#include "mallard/recognizer/symbol.hpp"

#include <vector>

using std::vector;

namespace mallard {
namespace recognizer {

DFARecognizer symbol() {
    static const vector<State> STATES = {
        {false, {
            {'+', 2}, {'-', 2}, {'*', 3}, {'/', 2}, {'?', 4}, {'.', 1}, {'!', 5}, {'~', 2}, {'|', 6}, {'&', 7}, {'^', 2}, {'<', 8}, {'>', 9}, {':', 10}, {'=', 11}, 
            {'[', 1}, {']', 1}, {'{', 1}, {'}', 1}, {'(', 1}, {')', 1}, {',', 1}, {';', 1}, {'\\', 1}}
        },
        {true, {}},
        {true, {{'=', 1}, {'>', 1}}},
        {true, {{'*', 2}, {'=', 1}}},
        {true, {{':', 1}, {'.', 1}}},
        {true, {{'!', 1}, {'=', 1}}},
        {true, {{'|', 1}, {'=', 1}}},
        {true, {{'&', 1}, {'=', 1}}},
        {true, {{'<', 2}, {'=', 1}}},
        {true, {{'>', 2}, {'=', 1}}},
        {true, {{':', 1}}},
        {true, {{'=', 1}}}
    };

    return DFARecognizer(1, STATES);
}

}
}