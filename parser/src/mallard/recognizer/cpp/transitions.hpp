#pragma once

#define DIGITS(x) {'0', x}, {'1', x}, {'2', x}, {'3', x}, {'4', x}, {'5', x}, {'6', x}, {'7', x}, {'8', x}, {'9', x}
#define HEX_DIGITS(x) {'a', x}, {'b', x}, {'c', x}, {'d', x}, {'e', x}, {'f', x}, {'A', x}, {'B', x}, {'C', x}, {'D', x}, {'E', x}, {'F', x}
#define ALPHA_LOWER(x) \
    {'a', x}, {'b', x}, {'c', x}, {'d', x}, {'e', x}, {'f', x}, {'g', x}, {'h', x}, {'i', x}, {'j', x}, \
    {'k', x}, {'l', x}, {'m', x}, {'n', x}, {'o', x}, {'p', x}, {'q', x}, {'r', x}, {'s', x}, {'t', x}, \
    {'u', x}, {'v', x}, {'w', x}, {'x', x}, {'y', x}, {'z', x}
#define ALPHA_UPPER(x) \
    {'A', x}, {'B', x}, {'C', x}, {'D', x}, {'E', x}, {'F', x}, {'G', x}, {'H', x}, {'I', x}, {'J', x}, \
    {'K', x}, {'L', x}, {'M', x}, {'N', x}, {'O', x}, {'P', x}, {'Q', x}, {'R', x}, {'S', x}, {'T', x}, \
    {'U', x}, {'V', x}, {'W', x}, {'X', x}, {'Y', x}, {'Z', x}
