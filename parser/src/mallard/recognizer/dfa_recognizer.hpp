#pragma once

#include "recognizer.hpp"

#include <cstdint>
#include <sstream>
#include <unordered_map>
#include <vector>

namespace mallard {
namespace recognizer {

struct State {
    bool is_accepting;
    std::unordered_map<char, std::size_t> transitions;
};

struct DFARecognizer : public Recognizer {
    DFARecognizer(std::size_t n_matches, const std::vector<State>& states);
    DFARecognizer(DFARecognizer&& other);
    virtual ~DFARecognizer();

    virtual void munch(char ch);

    virtual bool has_accepted() const {
        return states[current].is_accepting;
    }
    
    virtual std::vector<std::string> matches() const;

protected:
    std::vector<std::stringstream> buffers;
    const std::vector<State>& states;
    std::size_t current;
};

}
}