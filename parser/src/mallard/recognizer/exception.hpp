#pragma once

#include "mallard/exception/mallard_exception.hpp"

namespace mallard {
namespace recognizer {

class RecognizerException : public exception::MallardException {
public:
    RecognizerException(const std::string& what_arg);	
    RecognizerException(const char* what_arg);
    RecognizerException(const RecognizerException& other) noexcept;
};

}
}