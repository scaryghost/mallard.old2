#pragma once

#include "int_literal.hpp"

namespace mallard {
namespace recognizer {

struct FloatLiteral : public Recognizer {
    FloatLiteral();
    virtual ~FloatLiteral();

    virtual void munch(char ch);
    virtual bool has_accepted() const {
        return _state == State::FRACTIONAL && fractionalRecognizer.has_accepted();
    }
    virtual std::vector<std::string> matches() const {
        return { intergralRecognizer.matches()[0], fractionalRecognizer.matches()[0] };
    }

private:
    enum State : std::uint8_t {
        INTERGRAL,
        FRACTIONAL
    };

    DFARecognizer intergralRecognizer, fractionalRecognizer;
    State _state;
};

}
}