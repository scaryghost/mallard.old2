#pragma once

#include "one_but_not.hpp"
#include "dfa_recognizer.hpp"

namespace mallard {
namespace recognizer {

struct Identifier : public OneButNot {
    Identifier();
    virtual ~Identifier();

private:
    DFARecognizer keyword_recognizer, identifier_recognizer;
};

}
}