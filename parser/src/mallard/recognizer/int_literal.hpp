#pragma once

#include "one_of.hpp"
#include "dfa_recognizer.hpp"

namespace mallard {
namespace recognizer {

struct IntLiteral : public OneOf {
    IntLiteral();
    virtual ~IntLiteral();

private:
    DFARecognizer binaryRecognizer, decimalRecognizer, hexRecognizer, octRecognizer;
};

DFARecognizer int_binary_literal();
DFARecognizer int_decimal_literal();
DFARecognizer int_hex_literal();
DFARecognizer int_oct_literal();

}
}