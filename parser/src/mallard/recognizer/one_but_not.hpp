#pragma once

#include "recognizer.hpp"
#include "one_of.hpp"

#include <initializer_list>

namespace mallard {
namespace recognizer {

struct OneButNot : public Recognizer {
    OneButNot(Recognizer* target, const std::initializer_list<Recognizer*>& others);
    virtual ~OneButNot();

    virtual void munch(char ch);

    virtual bool has_accepted() const {
        return (!others.has_accepted() || others_rejected) && target->has_accepted();
    }
    virtual std::vector<std::string> matches() const {
        return target->matches();
    }

private:
    OneOf others;
    Recognizer* target;
    bool others_rejected;
};

}
}