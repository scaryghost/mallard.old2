#pragma once

#include "recognizer.hpp"

#include <initializer_list>

namespace mallard {
namespace recognizer {

struct OneOf : public Recognizer {
    OneOf(const std::initializer_list<Recognizer*>& recognizers);
    virtual ~OneOf();

    virtual void munch(char ch);

    std::vector<Recognizer*> accepted_recognizers() const;
    virtual bool has_accepted() const {
        return accepted_recognizers().size() == 1;
    }
    virtual std::vector<std::string> matches() const {
        return _recognizers[0]->matches();
    }

private:
    std::vector<Recognizer*> _recognizers;
};

}
}