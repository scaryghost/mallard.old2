#pragma once

#include <vector>
#include <string>

namespace mallard {
namespace recognizer {

struct Recognizer {
    virtual ~Recognizer();

    virtual void munch(char ch) = 0;

    virtual bool has_accepted() const = 0;
    virtual std::vector<std::string> matches() const = 0;
};

}
}