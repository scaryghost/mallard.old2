#pragma once

#include "recognizer.hpp"

#include <cstddef>
#include <sstream>

namespace mallard {
namespace recognizer {

struct StringLiteral : public Recognizer {
    StringLiteral();
    virtual ~StringLiteral();

    virtual void munch(char ch);

    virtual bool has_accepted() const {
        return state == State::END_STR;
    }
    virtual std::vector<std::string> matches() const {
        return { buffer.str() };
    }

private:
    enum class State {
        EMPTY,
        READING_CHARS,
        END_STR
    };

    State state;
    std::stringstream buffer;
};

}
}