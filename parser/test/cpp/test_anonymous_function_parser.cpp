#include "catch2/catch.hpp"

#include "mallard/grammar/binary_expression.hpp"
#include "mallard/grammar/operands.hpp"
#include "mallard/parser/expression.hpp"
#include "tokens.hpp"

#include <cstddef>
#include <cstring>
#include <sstream>
#include <tuple>

using namespace std;
using namespace mallard;
using namespace mallard::parser;

const mallard::parser::Token var_sub3 = {{"sub3"}, mallard::parser::TokenType::IDENTIFIER},
    var_add5 = {{"add5"}, mallard::parser::TokenType::IDENTIFIER},
    literal_int_3 = {{"3"}, mallard::parser::TokenType::INT_LITERAL},
    literal_int_5 = {{"5"}, mallard::parser::TokenType::INT_LITERAL};

TEST_CASE("anonymous functions", "[parser_anonymous_function]" ) {
    auto data = GENERATE(values<tuple<vector<const Token*>, const char*>>({
        { {&symbol_fwd_slash, &var_x, &symbol_fwd_slash, &var_y, &symbol_arrow, &var_x, &op_plus, &var_y}, "\\x \\y -> (x + y)" },
        { {&symbol_open_paren, &symbol_close_paren, &symbol_arrow, &literal_int_1}, "() -> 1" },
        { {&symbol_open_paren, &symbol_close_paren, &symbol_arrow, &symbol_open_brace, &symbol_close_brace}, "() -> {}" },
        { {
            &symbol_fwd_slash, &var_x, &symbol_arrow, &symbol_open_brace,
              &keyword_val, &var_sub3, &op_equals, &symbol_fwd_slash, &var_y, &symbol_arrow, &var_y, &op_minus, &literal_int_3,
              &keyword_val, &var_add5, &op_equals, &symbol_fwd_slash, &var_y, &symbol_arrow, &var_y, &op_plus, &literal_int_5,
              &var_add5, &symbol_open_paren, &var_x, &symbol_close_paren, &op_divide, &var_sub3, &symbol_open_paren, &var_x, &symbol_close_paren,
            &symbol_close_brace
          }, "\\x -> {\n    val sub3 = \\y -> (y - 3)\n    val add5 = \\y -> (y + 5)\n    (add5(x) / sub3(x))\n}"
        }
    }));

    Expression expression;
    stringstream os;
    
    for(auto& t: get<0>(data)) {
        expression.munch(*t);
    }
    expression.statement()->flatten(os);
    
    CHECK(os.str() == get<1>(data));
}
