#include "catch2/catch.hpp"

#include "mallard/grammar/binary_expression.hpp"
#include "mallard/grammar/operands.hpp"
#include "mallard/parser/expression.hpp"
#include "tokens.hpp"

#include <cstddef>
#include <cstring>
#include <sstream>

using namespace std;
using namespace mallard;
using namespace mallard::parser;

struct ExpressionData{
    vector<const Token*> tokens;
    const char* expected;
};

TEST_CASE("expressions", "[parser_expression]" ) {
    auto data = GENERATE(values<ExpressionData>({
        { {&var_x}, "x" },
        { {&op_minus, &var_x}, "-(x)" },
        { {&op_minus, &op_minus, &var_y}, "-(-(y))" },
        { {&var_x, &op_plus, &var_y}, "(x + y)" },
        { {&var_x, &op_minus, &op_minus, &var_y}, "(x - -(y))" },
        { {&var_x, &op_plus, &var_y, &op_multiply, &var_z}, "(x + (y * z))" },
        { {&var_x, &op_plus, &var_y, &op_plus, &var_z, &op_plus, &var_w}, "(((x + y) + z) + w)" },
        { {&var_x, &op_plus, &var_y, &op_multiply, &var_z, &op_divide, &var_w}, "(x + ((y * z) / w))" },
        { {&var_y, &op_multiply, &literal_float}, "(y * 2.71828)"},
        { {&op_minus, &literal_int, &op_divide, &literal_float}, "(-(12345) / 2.71828)"},
        { {&symbol_open_paren, &literal_int_1, &op_plus, &var_x, &symbol_close_paren, &op_divide, &var_y}, "(((1 + x)) / y)"}
    }));

    Expression expression;
    stringstream os;
    
    for(auto& t: data.tokens) {
        expression.munch(*t);
    }
    expression.statement()->flatten(os);
    
    CHECK(os.str() == data.expected);
}

/*
where:
        tokens | expected
        ["x"]  | "x"
        ["-", "x"] | "(-x)"
        ["-", "-", "y"] | "(-(-y))"
        ["x", "+", "y"] | "(x + y)"
        ["x", "-", "-", "y"] | "(x - (-y))"
        ["x", "+", "y", "*", "z"] | "(x + (y * z))"
        ["x", "+", "y", "+", "z", "+", "w"] | "(((x + y) + z) + w)"
        ["x", "+", "y", "*", "z", "/", "w"] | "(x + ((y * z) / w))"
        ["x", "*", "(", "y", "+", "z", ")", "/", "w"] | "((x * (y + z)) / w)"
        ["(", "x", "+", "y", ")", "*", "z"] | "((x + y) * z)"
        ["x", "+", "(", "y", "+", "z", ")"] | "(x + (y + z))"
        ["x", "*", "-", "(", "y", "+", "z", ")"] | "(x * (-(y + z)))"
        */