#include "catch2/catch.hpp"

#include "mallard/parser/expression.hpp"
#include "tokens.hpp"

#include <cstddef>
#include <cstring>
#include <sstream>
#include <tuple>

using namespace std;
using namespace mallard;
using namespace mallard::parser;

TEST_CASE("valid function calls", "[parser_function_expression]" ) {
    auto data = GENERATE(values<tuple<vector<const Token*>, const char*>>({
        { {&var_fib, &symbol_open_paren, &symbol_close_paren}, "fib()" },
        { {
            &var_fib, &symbol_open_paren, 
                &var_y, &op_multiply, &literal_float, &symbol_comma,
                &op_minus, &literal_int, &op_divide, &literal_float,
            &symbol_close_paren
          }, "fib((y * 2.71828), (-(12345) / 2.71828))" },
        { {
            &var_fib, &symbol_open_paren, 
                &var_x, &op_minus, &op_minus, &var_y,
            &symbol_close_paren
          }, "fib((x - -(y)))" },
        { {
            &var_fib, &symbol_open_paren, 
                &var_x, &op_minus, &literal_int_1, &symbol_comma,
                &var_fib, &symbol_open_paren,
                    &var_x, &symbol_comma,
                    &var_y, &op_minus, &literal_int_1,
                &symbol_close_paren,
            &symbol_close_paren
            }, "fib((x - 1), fib(x, (y - 1)))" },
        { {
            &var_fib, &symbol_open_paren, &var_x, &symbol_close_paren, &symbol_open_paren, &var_y, &symbol_close_paren
            }, "fib(x)(y)" },
        { {
            &symbol_open_paren, &symbol_fwd_slash, &var_x, &symbol_fwd_slash, &var_y, &symbol_arrow, &var_x, &symbol_close_paren, &symbol_open_paren, &literal_int_1, &symbol_close_paren
            }, "(\\x \\y -> x)(1)" }
    }));

    Expression expression;
    stringstream os;
    
    for(auto& t: get<0>(data)) {
        expression.munch(*t);
    }
    expression.statement()->flatten(os);
    
    CHECK(os.str() == get<1>(data));
}