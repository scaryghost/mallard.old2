#include "catch2/catch.hpp"

#include "mallard/parser/expression.hpp"
#include "tokens.hpp"

#include <cstddef>
#include <cstring>
#include <sstream>
#include <tuple>

using namespace std;
using namespace mallard;
using namespace mallard::parser;

TEST_CASE("valid list expressions", "[parser_list_expression]" ) {
    auto data = GENERATE(values<tuple<vector<const Token*>, const char*>>({
        { {&symbol_open_bracket, &symbol_close_bracket}, "[]" },
        { {&symbol_open_bracket, &var_x, &symbol_close_bracket}, "[x]" },
        { {&symbol_open_bracket, &symbol_open_bracket, &symbol_close_bracket, &symbol_close_bracket}, "[[]]" },
        { {
            &symbol_open_bracket, 
            &var_x, &op_minus, &op_minus, &var_y, &symbol_comma, 
            &var_x, &op_plus, &var_y, &op_multiply, &var_z, &op_divide, &var_w, &symbol_comma, 
            &op_minus, &literal_int, &op_divide, &literal_float,
            &symbol_close_bracket
          }, "[(x - -(y)), (x + ((y * z) / w)), (-(12345) / 2.71828)]" },
        { {
            &symbol_open_bracket, 
            &symbol_open_paren, &literal_int_1, &op_plus, &var_x, &symbol_close_paren, &op_divide, &var_y, &symbol_comma,
            &symbol_open_bracket,
                &var_z, &symbol_comma,
                &literal_int_0,
            &symbol_close_bracket, &symbol_comma,
            &var_w,
            &symbol_close_bracket,
            &op_plus,
            &symbol_open_bracket,
            &symbol_close_bracket
        }, "([(((1 + x)) / y), [z, 0], w] + [])" }
    }));

    Expression expression;
    stringstream os;
    
    for(auto& t: get<0>(data)) {
        expression.munch(*t);
    }
    expression.statement()->flatten(os);
    CHECK(os.str() == get<1>(data));
}