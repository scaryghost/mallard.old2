#include "catch2/catch.hpp"

#include "mallard/recognizer/float_literal.hpp"
#include "mallard/recognizer/exception.hpp"

#include <cstddef>
#include <cstring>

using namespace std;
using namespace mallard::recognizer;

static vector<string> separate(const char* input) {
    vector<string> result;
    string temp;

    for (size_t i = 0; i < strlen(input); i++) {
        switch(input[i]) {
        case '.':
            result.push_back(temp);
            temp = "";
            break;
        default:
            temp += input[i];
            break;
        }
    }
    result.push_back(temp);

    return result;
}

TEST_CASE("valid floats", "[recognizer_float_literal]" ) {
    auto input = GENERATE("3.141592653", "0.6180339887", "23.1406926328", "0.00000");
    FloatLiteral recognizer;

    for(size_t i = 0; i < strlen(input); i++) {
        recognizer.munch(input[i]);
    }
    
    CHECK(recognizer.has_accepted());
    CHECK(recognizer.matches() == separate(input));
}

TEST_CASE("invalid floats", "[recognizer_float_literal]" ) {
    auto input = GENERATE(".5", "2.7182818285f", "1e9");
    FloatLiteral recognizer;

    auto expression = [&recognizer, &input]() {
        for(size_t i = 0; i < strlen(input); i++) {
            recognizer.munch(input[i]);
        }
    };
    
    CHECK_THROWS_AS(expression(), RecognizerException);
}

TEST_CASE("incomplete floats", "[recognizer_float_literal]" ) {
    auto input = GENERATE("01234", "56789.");
    FloatLiteral recognizer;

    auto expression = [&recognizer, &input]() {
        for(size_t i = 0; i < strlen(input); i++) {
            recognizer.munch(input[i]);
        }
    };
    
    CHECK(!recognizer.has_accepted());
}