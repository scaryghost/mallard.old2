#include "catch2/catch.hpp"

#include "mallard/recognizer/identifier.hpp"

#include <cstddef>
#include <cstring>
#include <stdexcept>

using namespace std;
using namespace mallard::recognizer;

TEST_CASE("valid identifiers", "[recognizer_identifier]" ) {
    auto input = GENERATE("x", "_20", "var1", "snake_case", "camelCase", "PascalCase");
    Identifier recognizer;

    for(size_t i = 0; i < strlen(input); i++) {
        recognizer.munch(input[i]);
    }
    
    CHECK(recognizer.has_accepted());
    CHECK(recognizer.matches()[0] == input);
}

TEST_CASE("invalid identifiers", "[recognizer_identifier]" ) {
    auto input = GENERATE("2", "!", "var\"", "snake_case;", "*camelCase*", "Pasc@lCase");
    Identifier recognizer;

    auto expression = [&recognizer, &input]() {
        for(size_t i = 0; i < strlen(input); i++) {
            recognizer.munch(input[i]);
        }
    };
    
    CHECK_THROWS_AS(expression(), runtime_error);
}

TEST_CASE("keywords are not identifiers", "[recognizer_identifier]" ) {
    auto input = GENERATE("val");
    Identifier recognizer;

    for(size_t i = 0; i < strlen(input); i++) {
        recognizer.munch(input[i]);
    }
    
    CHECK(!recognizer.has_accepted());
}