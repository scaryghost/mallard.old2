#include "catch2/catch.hpp"

#include "mallard/recognizer/int_literal.hpp"
#include "mallard/recognizer/exception.hpp"

#include <cstddef>
#include <cstring>

using namespace std;
using namespace mallard::recognizer;

TEST_CASE("valid integers", "[recognizer_int_literal]" ) {
    auto input = GENERATE("0123456789", "010110111111b", "10100100000b", "0xdeadbeef", "644o");
    IntLiteral recognizer;

    for(size_t i = 0; i < strlen(input); i++) {
        recognizer.munch(input[i]);
    }
    
    CHECK(recognizer.has_accepted());
    CHECK(recognizer.matches()[0] == input);
}

TEST_CASE("invalid integers", "[recognizer_int_literal]" ) {
    auto input = GENERATE("01110201b", "0xabcdefg", "xcafebabe", "3.14159", "1e5", "-2468", "123def456");
    IntLiteral recognizer;

    auto expression = [&recognizer, &input]() {
        for(size_t i = 0; i < strlen(input); i++) {
            recognizer.munch(input[i]);
        }
    };
    
    CHECK_THROWS_AS(expression(), RecognizerException);
}