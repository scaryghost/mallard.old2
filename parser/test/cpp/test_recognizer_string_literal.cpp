#include "catch2/catch.hpp"

#include "mallard/recognizer/string_literal.hpp"

#include <cstddef>
#include <cstring>
#include <stdexcept>

using namespace std;
using namespace mallard::recognizer;

static string extract(const char* input) {
    string result;

    for (size_t i = 0; i < strlen(input); i++) {
        if (input[i] != '\"') {
            result += input[i];
        }
    }
    
    return result;
}

TEST_CASE("valid strings", "[recognizer_string]" ) {
    auto input = GENERATE(
        "\"the \'quick\' brown fox \\njumps over the lazy dog\"",
        "\"!@#$%^&*()[]\"",
        "\"val\"",
        "\"if\""
    );
    StringLiteral recognizer;

    for(size_t i = 0; i < strlen(input); i++) {
        recognizer.munch(input[i]);
    }
    
    CHECK(recognizer.has_accepted());
    CHECK(recognizer.matches()[0] == extract(input));
}
