#include "catch2/catch.hpp"

#include "mallard/recognizer/symbol.hpp"

#include <cstddef>
#include <cstring>
#include <stdexcept>

using namespace std;
using namespace mallard::recognizer;

TEST_CASE("valid binary operators", "[recognizer_symbol]" ) {
    auto input = GENERATE(
        "+", "-", "*", "/", "**",
        "=", "+=", "-=", "*=", "/=", "**=",
        "~", "|", "&", "^", "<<", "<<", 
        "|=", "&=", "^=", "<<=", ">>=",
        "<", "<=", ">", ">=", "!", "==", "!=",
        "||", "&&", "!",
        "!!", "?.", "?:",
        ":", "?",
        "::",
        "."
    );
    DFARecognizer recognizer = symbol();

    for(size_t i = 0; i < strlen(input); i++) {
        recognizer.munch(input[i]);
    }
    
    CHECK(recognizer.has_accepted());
    CHECK(recognizer.matches()[0] == input);
}
