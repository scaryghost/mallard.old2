#include "catch2/catch.hpp"

#include "mallard/grammar/statement.hpp"
#include "mallard/reader/stream.hpp"

#include <memory>
#include <sstream>
#include <tuple>

using namespace std;
using mallard::grammar::Statement;
using mallard::reader::StatementStream;

TEST_CASE("expression stream", "[stream]" ) {
    auto data = GENERATE(values<tuple<const char*, const char*>>({
        { "x", "x" },
        { "-x", "-(x)" },
        { "--y", "-(-(y))" },
        { "x\n+\ry", "(x + y)" },
        { "x--y", "(x - -(y))" },
        { "x +\ty * z", "(x + (y * z))" },
        { "x + y + z + w", "(((x + y) + z) + w)" },
        { "x + y * z / w", "(x + ((y * z) / w))" },
        { "val y = x", "val y = x" },
        { "val y = -x", "val y = -(x)" },
        { "val y = --y", "val y = -(-(y))" },
        { "val y = x\n+\ry", "val y = (x + y)" },
        { "val y = x--y", "val y = (x - -(y))" },
        { "val y = x +\ty * z", "val y = (x + (y * z))" },
        { "val y = x + y + z + w", "val y = (((x + y) + z) + w)" },
        { "val y = x + y * z / w", "val y = (x + ((y * z) / w))" },
        { "val sub3 = \\x -> x - 3", "val sub3 = \\x -> (x - 3)" },
        { "val if = \\p \\a \\b -> p(a)(b)", "val if = \\p \\a \\b -> p(a)(b)"},
        { R"(values.map(\x -> abs(x)).fold(0, \acc \e -> acc + e))", "((values . map(\\x -> abs(x))) . fold(0, \\acc \\e -> (acc + e)))" },
        { R"(students.filter(\x -> x.age < 21))", "(students . filter(\\x -> ((x . age) < 21)))" },
        { "values[3]", "values[3]" },
        { "grid[x][y]", "grid[x][y]" }
    }));

    stringstream input, os;
    input << get<0>(data);

    shared_ptr<Statement> statement;
    StatementStream stream(input);
    stream >> statement;

    statement->flatten(os);
    CHECK(os.str() == get<1>(data));
}