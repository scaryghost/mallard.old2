#include "catch2/catch.hpp"

#include "mallard/parser/no_such_element.hpp"
#include "mallard/parser/tokenizer.hpp"

#include <cstddef>
#include <sstream>
#include <stdexcept>
#include <vector>

using namespace std;
using namespace mallard::parser;

namespace Catch {
    template<>
    struct StringMaker<Token> {
        static string convert( Token const& value ) {
            stringstream buffer;
            buffer << value;

            return buffer.str();
        }
    };
}

struct Data {
    const char* input;
    vector<Token> expected;
};

TEST_CASE("valid tokens", "[tokenizer]" ) {
    auto data = GENERATE(values<Data>({
        {
            "x+y-z*w",
            {{{"x"}, TokenType::IDENTIFIER}, {{"+"}, TokenType::SYMBOL}, {{"y"}, TokenType::IDENTIFIER}, {{"-"}, TokenType::SYMBOL}, {{"z"}, TokenType::IDENTIFIER}, {{"*"}, TokenType::SYMBOL}, {{"w"}, TokenType::IDENTIFIER}}
        },
        {
            "a--b",
            {{{"a"}, TokenType::IDENTIFIER}, {{"-"}, TokenType::SYMBOL}, {{"-"}, TokenType::SYMBOL}, {{"b"}, TokenType::IDENTIFIER}}
        },
        {
            "true || false",
            {{{"true"}, TokenType::IDENTIFIER}, {{"||"}, TokenType::SYMBOL}, {{"false"}, TokenType::IDENTIFIER}}
        },
        {
            "active ? f : g",
            {{{"active"}, TokenType::IDENTIFIER}, {{"?"}, TokenType::SYMBOL}, {{"f"}, TokenType::IDENTIFIER}, {{":"}, TokenType::SYMBOL}, {{"g"}, TokenType::IDENTIFIER}}
        },
        {
            "val\ng =\t9.80665",
            {{{"val"}, TokenType::KEYWORD}, {{"g"}, TokenType::IDENTIFIER}, {{"="}, TokenType::SYMBOL}, {{"9", "80665"}, TokenType::FLOAT_LITERAL}}
        },
        {
            "val\rparagraph = \"the quick brown fox\"",
            {{{"val"}, TokenType::KEYWORD}, {{"paragraph"}, TokenType::IDENTIFIER}, {{"="}, TokenType::SYMBOL}, {{"the quick brown fox"}, TokenType::STRING_LITERAL}}
        }
    }));
        
    stringstream stream;
    stream << data.input;

    Tokenizer tokenizer(stream);
    vector<Token> tokens;
    
    while(true) {
        try {
            Token token;
            tokenizer >> token;

            tokens.push_back(token);
        } catch (const NoSuchElementError& e) {
            break;
        }

    }
    
    CHECK(tokens == data.expected);
}
